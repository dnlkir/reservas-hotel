var FormInputMask = function () {
    
    var handleInputMasks = function () {
        
        $(".price_mask").inputmask({
            mask: '999999999999.99',
            numericInput: true,
            placeholder: ' ',
            autoUnmask: true,
            greedy: false,
            clearMaskOnLostFocus: true,
            onBeforeMask: function (value) {
              // Value is stripped of decimals here!
              return value;
            },
            onUnMask: function (maskedValue, unmaskedValue) {
              return (Number(unmaskedValue) / 100).toFixed(2);
            }
          });

   }

    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        FormInputMask.init(); // init metronic core componets
    });
}