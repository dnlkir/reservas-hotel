@extends('metronic_layouts.auth')

@section('title')
{{trans('strings.reset_password')}}
@endsection

@section('content')
<!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="" role="form" method="post" action="{{ url('/password/email') }}">
        {{ csrf_field() }}
        <h3>{{ trans('strings.password_forgot') }}</h3>
        <p>{{ trans('strings.password_forgot_label') }}</p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" placeholder="{{ trans('strings.email') }}" id="email" type="email" name="email" value="{{ old('email') }}" required/> 
            </div>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-actions">
            <a href="{{ url('/login') }}" class="btn blue btn-outline">{{ trans('strings.login') }}</a>
            <button type="submit" class="btn green pull-right">{{ trans('strings.submit') }}</button>
        </div>
    </form>
<!-- END FORGOT PASSWORD FORM -->
@endsection
