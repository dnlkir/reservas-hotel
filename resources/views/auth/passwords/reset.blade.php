@extends('metronic_layouts.auth')

@section('title')
{{trans('strings.reset_password')}}
@endsection

@section('content')
<!-- BEGIN RECOVERY PASSWORD FORM -->
    <form class="" role="form" method="POST" action="{{ url('/password/reset') }}">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">

        <h3>{{ trans('strings.reset_password') }}</h3>
        <p>{{ trans('strings.reset_password_label') }}</p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" placeholder="{{ trans('strings.email') }}" id="email" type="email" name="email" value="{{ $email or old('email') }}" required autofocus>
            </div>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" placeholder="{{ trans('strings.password') }}" id="password" type="password" name="password" required>
            </div>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <div class="controls">
                <div class="input-icon">
                    <i class="fa fa-check"></i>
                    <input class="form-control placeholder-no-fix" placeholder="{{ trans('strings.password_confirmation') }}"  id="password-confirm" type="password" name="password_confirmation" required>
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-actions">
            <a href="{{ url('/login') }}" class="btn blue btn-outline">{{ trans('strings.login') }}</a>
            <button type="submit" class="btn green pull-right">{{ trans('strings.submit') }}</button>
        </div>
    </form>
<!-- END RECOVERY PASSWORD FORM -->
@endsection
