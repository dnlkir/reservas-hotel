@extends('metronic_layouts.auth')

@section('title')
{{trans('strings.register')}}
@endsection

@section('content')
<!-- BEGIN REGISTER FORM -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <h3 class="form-title">{{ trans('strings.register') }}</h3>
        <p>{{ trans('strings.register_label') }}</p>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.name')}}</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input id="name" type="text" class="form-control placeholder-no-fix" placeholder="{{trans('strings.name')}}" name="name" value="{{ old('name') }}" required autofocus /> 
            </div>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.username')}}</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input id="username" type="text" class="form-control placeholder-no-fix" placeholder="{{trans('strings.username')}}" name="username" value="{{ old('username') }}" required autofocus /> 
            </div>
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.email')}}</label>
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input id="email" type="email" class="form-control placeholder-no-fix" placeholder="{{trans('strings.email')}}" name="email" value="{{ old('email') }}" required autofocus /> 
            </div>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.password')}}</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input id="password" type="password" class="form-control placeholder-no-fix" placeholder="{{trans('strings.password')}}" name="password" required/> 
            </div>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.password_confirmation')}}</label>
            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input id="password_confirmation" type="password" class="form-control placeholder-no-fix" placeholder="{{trans('strings.password_confirmation')}}" name="password_confirmation" required/> 
            </div>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-actions">
            <a href="{{ url('/login') }}" class="btn blue btn-outline">{{ trans('strings.login') }}</a>
            <button type="submit" class="btn green pull-right">{{ trans('strings.submit') }}</button>
        </div>
    </form>
<!-- END REGISTER FORM -->
@endsection
