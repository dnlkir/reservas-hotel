@extends('metronic_layouts.auth')

@section('title')
{{trans('strings.login')}}
@endsection

@section('content')
<!-- BEGIN LOGIN FORM -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <h3 class="form-title">{{trans('strings.login_label')}}</h3>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.email_or_username')}}</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input id="email" class="form-control placeholder-no-fix" placeholder="{{trans('strings.email_or_username')}}" name="email" value="{{ old('email') }}" required autofocus /> 
            </div>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">{{trans('strings.password')}}</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input id="password" type="password" class="form-control placeholder-no-fix" placeholder="{{trans('strings.password')}}" name="password" required/> 
            </div>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-actions">
            <button type="submit" class="btn green pull-right">{{trans('strings.login')}}</button>
        </div>
        <br><br>
    </form>
<!-- END LOGIN FORM -->
@endsection
