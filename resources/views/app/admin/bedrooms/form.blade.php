{{ Form::hidden('id') }}
<div class="form-body">
	<div class="form-group">
        <label class="col-md-3 control-label">Piso</label>
        <div class="col-md-4">
        	{!! Form::select('floor_id', $floors, null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Número de Personas</label>
        <div class="col-md-4">
            {!! Form::number('number_people', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Nombre</label>
        <div class="col-md-4">
        	{!! Form::text('name', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Descripción</label>
        <div class="col-md-4">
        	{!! Form::text('description', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Precio</label>
        <div class="col-md-4">
            {!! Form::number('price', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Servicios</label>
        <div class="col-md-4">
            {!! Form::select('service_id[]', $services, isset($bedroom_service) ? $bedroom_service : null, array(
            'multiple' => true, 'class' => 'multi-select', 'id' => 'my_multi_select1')) !!} 
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Foto</label>
        <div class="col-md-4">
        	<div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img 
                    @if($bedroom->picture != '')
                    src="{{ $bedroom->picture }}" 
                    @else
                        src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                    @endif
                    /> 
                </div>
        		<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                <div>
                    <span class="btn default btn-file">
                        <span class="fileinput-new">{{ trans('strings.select_image') }}</span>
                        <span class="fileinput-exists">{{ trans('strings.change_image') }}</span>
                        <input data-max-size="1048576" type="file" name="picture" accept="image/*">
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">{{ trans('strings.remove_image') }}</a>
                </div>
    		</div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit($button, ['class' => 'btn btn-circle green']) !!}
        </div>
    </div>
</div>