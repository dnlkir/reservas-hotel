@extends('metronic_layouts.partials.form')

@section('form_body')

	@if($bedroom->id>0)		

		@section('title')
			{{ trans('strings.edit_table', ['table' => 'Habitación']) }}
		@endsection	

		{{ Form::model($bedroom, ['route' => ['admin.bedrooms.update', $bedroom->id], 'method' => 'put','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.bedrooms.form', ['button' => Lang::get('strings.edit') ])
	
	@else

		@section('title')
			{{ trans('strings.create_table', ['table' => 'Habitación']) }}
		@endsection	

		{{ Form::open(['route' => 'admin.bedrooms.store','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.bedrooms.form', ['button' => Lang::get('strings.create') ])						
		
	@endif

	{{ Form::close() }}

@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <a href="{{ url('/admin/bedrooms') }}">Habitación</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection