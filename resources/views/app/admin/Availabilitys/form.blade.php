{{ Form::hidden('id') }}
<div class="form-body" style="height:300px">

    @foreach($bedrooms as $bedroom)
  
    @if(count($bedroom->reservations_availability))

    @foreach($bedroom->reservations_availability as $reservation)

    @if($reservation->status == 'reserva')

    <div class="col-md-2" style="padding-bottom: 2%">
        <button type="button" class="btn" style="background-color: purple; color:white" data-toggle="modal" data-target="#myModal{{$bedroom->id}}">
            {{$bedroom->name}}
        </button> 
    </div>

    @elseif($reservation->status == 'ocupada')

    <div class="col-md-2" style="padding-bottom: 2%">
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal{{$bedroom->id}}">
            {{$bedroom->name}}
        </button> 
    </div>

    @elseif($reservation->status == 'entregada')

    <div class="col-md-2" style="padding-bottom: 2%">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal2{{$bedroom->id}}">
            {{$bedroom->name}}
        </button> 
    </div>

    @endif
    
    <!-- Modal -->
    <div class="modal fade" id="myModal{{$bedroom->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Información de Reserva {{$bedroom->name}}</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>
                            <strong>Nombre:</strong>
                            {{ $reservation->name }}
                        </li>
                        <li>
                            <strong>Documento de identidad:</strong>
                            {{ $reservation->identification_document }}
                        </li>
                        <li>
                            <strong>Teléfono:</strong>
                            {{ $reservation->phone }}
                        </li>
                        <li>
                            <strong>Email:</strong>
                            {{ $reservation->email }}
                        </li>
                        <li>
                            <strong>Número de personas:</strong>
                            {{ $reservation->amount_people }}
                        </li>
                        <li>
                            <strong>Fecha de entrada:</strong>
                            {{ $reservation->entry_date }}
                        </li>
                        <li>
                            <strong>Fecha de salida:</strong>
                            {{ $reservation->departure_date }}
                        </li>
                        <li>
                            <strong>Precio total:</strong>
                            {{ $reservation->total_price }}
                        </li>
                        <li>
                            <strong>Información acompañante:</strong>
                            {{ $reservation->information_accompanist }}
                        </li>
                        <br>
                        <li>
                            <strong>Estado:</strong>
                            {{ $reservation->status }}
                        </li>
                    </ul>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal2 -->
    <div class="modal fade" id="myModal2{{$bedroom->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Información de Reserva {{$bedroom->name}}</h4>
                </div>
                <div class="modal-body">
                    No existe reserva para esta habitación <br><br>
                    <a href="{{ url('/admin/reservations/'.$bedroom->id.'/create_reservation') }}" class="btn blue">Reservar Ahora</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    {{-- End count reservation --}}
    @endforeach

    {{-- End count --}}
    @else

    <div class="col-md-2" style="padding-bottom: 2%">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal{{$bedroom->id}}">
            {{$bedroom->name}}
        </button> 
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal{{$bedroom->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Información de Reserva {{$bedroom->name}}</h4>
                </div>
                <div class="modal-body">
                    No existe reserva para esta habitación <br><br>
                    <a href="{{ url('/admin/reservations/'.$bedroom->id.'/create_reservation') }}" class="btn blue">Reservar Ahora</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    @endif

    @endforeach

</div>
