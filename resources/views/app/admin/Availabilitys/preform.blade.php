@extends('metronic_layouts.partials.form')

@section('form_body')

		@section('title')
			Disponibilidad
		@endsection	

		{{ Form::open(['enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}

		@include('app.admin.availabilitys.form', ['button' => Lang::get('strings.edit') ])

		{{ Form::close() }} 

@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <a href="{{ url('/admin/bedrooms') }}">Habitaciones</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection