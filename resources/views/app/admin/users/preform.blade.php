@extends('metronic_layouts.partials.form')

@section('form_body')

	@if($user->id>0)		

		@section('title')
			{{ trans('strings.edit_table', ['table' => trans('strings.user')]) }}
		@endsection	

		{{ Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'put','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.users.form', ['button' => Lang::get('strings.edit') ])
	
	@else

		@section('title')
			{{ trans('strings.create_table', ['table' => trans('strings.user')]) }}
		@endsection	

		{{ Form::open(['route' => 'admin.users.store','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.users.form', ['button' => Lang::get('strings.create') ])						
		
	@endif

	{{ Form::close() }}

@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <a href="{{ url('/admin/users') }}">{{ trans('strings.users') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection