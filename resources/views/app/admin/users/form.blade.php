{{ Form::hidden('id') }}
<div class="form-body">
	<div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.name')}}</label>
        <div class="col-md-4">
        	{!! Form::text('name', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.username')}}</label>
        <div class="col-md-4">
        	{!! Form::text('username', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.email')}}</label>
        <div class="col-md-4">
        	{!! Form::text('email', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.password')}}</label>
        <div class="col-md-4">
        	{!! Form::password('password', ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.password_confirmation')}}</label>
        <div class="col-md-4">
        	{!! Form::password('password_confirmation', ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.roles')}}</label>
        <div class="col-md-4">
        	{!! Form::select('role_id[]', $roles, isset($roles_user) ? $roles_user : null, array(
			'multiple' => true, 'class' => 'multi-select', 'id' => 'my_multi_select1')) !!} 
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">{{trans('strings.picture')}}</label>
        <div class="col-md-4">
        	<div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img 
                    @if($user->picture != '')
                    src="{{ $user->picture }}" 
                    @else
                        src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                    @endif
                    /> 
                </div>
        		<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                <div>
                    <span class="btn default btn-file">
                        <span class="fileinput-new">{{ trans('strings.select_image') }}</span>
                        <span class="fileinput-exists">{{ trans('strings.change_image') }}</span>
                        <input data-max-size="1048576" type="file" name="file" accept="image/*">
                    </span>
                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">{{ trans('strings.remove_image') }}</a>
                </div>
    		</div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit($button, ['class' => 'btn btn-circle green']) !!}
        </div>
    </div>
</div>