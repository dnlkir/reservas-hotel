{{ Form::hidden('id') }}
<div class="form-body">
	<div class="form-group">
        <label class="col-md-3 control-label">Número</label>
        <div class="col-md-4">
        	{!! Form::number('number', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit($button, ['class' => 'btn btn-circle green']) !!}
        </div>
    </div>
</div>