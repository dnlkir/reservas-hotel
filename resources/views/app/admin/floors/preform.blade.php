@extends('metronic_layouts.partials.form')

@section('form_body')

	@if($floor->id>0)		

		@section('title')
			{{ trans('strings.edit_table', ['table' => 'Pisos']) }}
		@endsection	

		{{ Form::model($floor, ['route' => ['admin.floors.update', $floor->id], 'method' => 'put','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.floors.form', ['button' => Lang::get('strings.edit') ])
	
	@else

		@section('title')
			{{ trans('strings.create_table', ['table' => 'Pisos']) }}
		@endsection	

		{{ Form::open(['route' => 'admin.floors.store','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.floors.form', ['button' => Lang::get('strings.create') ])						
		
	@endif

	{{ Form::close() }}

@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <a href="{{ url('/admin/floors') }}">Pisos</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection