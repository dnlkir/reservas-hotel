{{ Form::hidden('id') }}
<div class="form-body">
    {{ Form::hidden('bedroom_id', $bedroom->id) }}
    {{ Form::hidden('amount_people', $bedroom->number_people) }}
    {{ Form::hidden('total_price', $bedroom->price) }}

    <div class="form-group">
        <label class="col-md-3 control-label">Nombre</label>
        <div class="col-md-4">
            {!! Form::text('name', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Documento de identidad</label>
        <div class="col-md-4">
            {!! Form::number('identification_document', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Cantidad de personas</label>
        <div class="col-md-4">
            {!! Form::text('amount_people', $bedroom->number_people, ['required', 'class'=>'form-control', 'disabled']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Teléfono</label>
        <div class="col-md-4">
            {!! Form::number('phone', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Email</label>
        <div class="col-md-4">
            {!! Form::email('email', null, ['required', 'class'=>'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Información acompañantes</label>
        <div class="col-md-4">
            {!! Form::textarea('information_accompanist', null, ['required', 'class'=>'form-control', 'rows'=> '3']) !!}
        </div>
    </div>

    <div class="form-group" style="z-index: 1">
        <label class="col-md-3 control-label">Fecha de reserva</label>
        
        <div class="col-md-4">
            <div class="input-group input-large date-picker input-daterange " data-date-format="yyyy/mm/dd" data-date-start-date="0d">
                {!! Form::text('entry_date', null, ['required', 'class'=>'form-control datepicker', 'autocomplete'=>'off']) !!}
                <span class="input-group-addon"> a </span>
                {!! Form::text('departure_date', null, ['required', 'class'=>'form-control datepicker', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>


</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit($button, ['class' => 'btn btn-circle green']) !!}
        </div>
    </div>
</div>