@extends('metronic_layouts.partials.form')

@section('form_body')

	@if($reservation->id>0)		

		@section('title')
			{{ trans('strings.edit_table', ['table' => 'Reservación']) }}
		@endsection	

		{{ Form::model($reservation, ['route' => ['admin.reservations.update', $reservation->id], 'method' => 'put','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.reservations.form', ['button' => Lang::get('strings.edit') ])
	
	@else

		@section('title')
			{{ trans('strings.create_table', ['table' => 'Reservación']) }}
		@endsection	

		{{ Form::open(['route' => 'admin.reservations.store','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.reservations.form_create', ['button' => Lang::get('strings.create') ])						
		
	@endif

	{{ Form::close() }}

@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <a href="{{ url('/admin/reservations') }}">Reservación</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection