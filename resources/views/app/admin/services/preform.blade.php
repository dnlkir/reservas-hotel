@extends('metronic_layouts.partials.form')

@section('form_body')

	@if($service->id>0)		

		@section('title')
			{{ trans('strings.edit_table', ['table' => 'Servicios']) }}
		@endsection	

		{{ Form::model($service, ['route' => ['admin.services.update', $service->id], 'method' => 'put','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.services.form', ['button' => Lang::get('strings.edit') ])
	
	@else

		@section('title')
			{{ trans('strings.create_table', ['table' => 'Servicios']) }}
		@endsection	

		{{ Form::open(['route' => 'admin.services.store','enctype'=>'multipart/form-data', 'class'=>'form-horizontal']) }}
		@include('app.admin.services.form', ['button' => Lang::get('strings.create') ])						
		
	@endif

	{{ Form::close() }}

@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <a href="{{ url('/admin/services') }}">Servicios</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection