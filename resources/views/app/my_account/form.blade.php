<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        {!! Form::text('name', null, ['required', 'class'=>'form-control', 'placeholder'=>Lang::get('strings.full_name')]) !!}
    </div>
</div>
<div class="form-group">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
		{!! Form::email('email', null, ['required', 'disabled','class'=>'form-control', 'placeholder'=>Lang::get('strings.email')]) !!}
	</div>
</div>
<div class="form-group">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-lock"></i></span>
		{!! Form::password('password', ['class'=>'form-control', 'placeholder'=>Lang::get('strings.password')]) !!}
	</div>
</div> 
<div class="form-group">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-lock"></i></span>
		{!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>Lang::get('strings.password_confirmation')]) !!}
	</div>
</div>
<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
        {!! Form::text('phone', null, ['required', 'class'=>'form-control', 'placeholder'=>Lang::get('strings.phone')]) !!}
    </div>
</div>
{!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
