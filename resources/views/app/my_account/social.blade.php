<div class="tiles">

    <div class="tile double btn-facebook">
        <div class="tile-body">
            <h4>Facebook</h4>
            @if($user->social_facebook)
            <p><code>{{$user->social_facebook->provider_user_email}}</code></p>
            <br>
            <p><code>{{$user->social_facebook->provider_user_id}}</code></p>
            <br>
            {!! Form::open(['url' => '/social_account/'.$user->social_facebook->social_account_id, 'method' => 'delete', 'class' => 'form-class']) !!}
                <button type="submit" class="btn red btn-xs">{{ Lang::get('strings.delete') }}</button>
            {!! Form::close() !!}
            @else
            <p></p>
            <br>
            <p></p>
            <br>
            <a href="/auth/facebook" class="btn btn-block red">
              {{ trans('strings.connect') }}
            </a>
            @endif
        </div>
    </div> 

    <div class="tile double btn-google">
        <div class="tile-body">
            <h4>Google</h4>
            @if($user->social_google)
            <p><code>{{$user->social_google->provider_user_email}}</code></p>
            <br>
            <p><code>{{$user->social_google->provider_user_id}}</code></p>
            <br>
            {!! Form::open(['url' => '/social_account/'.$user->social_google->social_account_id, 'method' => 'delete', 'class' => 'form-class']) !!}
                <button type="submit" class="btn blue btn-xs">{{ Lang::get('strings.delete') }}</button>
            {!! Form::close() !!}
            @else
            <p></p>
            <br>
            <p></p>
            <br>
            <a href="/auth/google" class="btn btn-block blue">
              {{ trans('strings.connect') }}
            </a>
            @endif
        </div>
    </div> 

</div>
