@extends('metronic_layouts.app')

@section('title')
{{trans('strings.my_account')}}
@endsection

@section('breadcrumb')
<li>
    <a href="{{ url('/home') }}">{{trans('strings.home')}}</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>@yield('title')</span>
</li>
@endsection

@push('styles')
<link href="/metronic/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('scripts')
<script src="/metronic/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/metronic/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/metronic_custom/pages/scripts/form-select2.js"></script>
@endpush

@section('content')
<div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="profile-sidebar">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet ">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic">
                                            <img class="img-responsive" alt=""
                                            @if(Auth::user()->picture != '')
                                            src="{{Auth::user()->picture}}" 
                                            @else
                                                src="/img/profile-placeholder.jpg"
                                            @endif /> 
                                        </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name">{{ $user->name }}</div>
                                            @foreach($user->roles as $role)
                                                <div class="profile-usertitle-job">{{ $role->display_name }}</div>
                                            @endforeach
                                        </div>
                                        <!-- END SIDEBAR USER TITLE -->                                       
                                    </div>
                                    <!-- END PORTLET MAIN -->
                                </div>
                                <!-- END BEGIN PROFILE SIDEBAR -->
                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">@yield('title')</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" class="uppercase bold" data-toggle="tab">{{ trans('strings.personal_info') }}</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_2" class="uppercase bold" data-toggle="tab">{{ trans('strings.change_image') }}</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_3" class="uppercase bold" data-toggle="tab">{{ trans('strings.change_password') }}</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                {!! Form::model($user, ['route' => ['my_account.update', $user->id], 'method' => 'put', 'enctype'=>'multipart/form-data']) !!}
                                                    {{ Form::hidden('id') }}
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">                                                            
                                                            <div class="form-group">
                                                                <label class="control-label">{{ trans('strings.name') }}</label>
                                                                {!! Form::text('name', null, ['required', 'class'=>'form-control']) !!}
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">{{ trans('strings.username') }}</label>
                                                                {!! Form::text('username', null, ['required', 'class'=>'form-control']) !!}
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">{{ trans('strings.email') }}</label>
                                                                {!! Form::email('email', null, ['required', 'readonly', 'class'=>'form-control']) !!} 
                                                            </div>    
                                                        </div>
                                                        <!-- END PERSONAL INFO TAB -->
                                                        <!-- CHANGE AVATAR TAB -->
                                                        <div class="tab-pane" id="tab_1_2">
                                                            <div class="form-group">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <img 
                                                                        @if(Auth::user()->picture != '')
                                                                        src="{{ Auth::user()->picture }}" 
                                                                        @else
                                                                            src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                                        @endif
                                                                        /> 
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new">{{ trans('strings.select_image') }}</span>
                                                                            <span class="fileinput-exists">{{ trans('strings.change_image') }}</span>
                                                                            <input data-max-size="1048576" type="file" name="file" accept="image/*">
                                                                        </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">{{ trans('strings.remove_image') }}</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END CHANGE AVATAR TAB -->
                                                        <!-- CHANGE PASSWORD TAB -->
                                                        <div class="tab-pane" id="tab_1_3">
                                                            <div class="form-group">
                                                                <label class="control-label">{{ trans('strings.password_current') }}</label>
                                                                {!! Form::password('password_current', ['class'=>'form-control']) !!}
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">{{ trans('strings.password') }}</label>
                                                                {!! Form::password('password', ['class'=>'form-control']) !!}
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">{{ trans('strings.password_confirmation') }}</label>
                                                                {!! Form::password('password_confirmation', ['class'=>'form-control']) !!} 
                                                            </div>
                                                        </div>
                                                        <!-- END CHANGE PASSWORD TAB -->
                                                    </div>
                                                    <div class="margiv-top-10">
                                                        {!! Form::submit(trans('strings.save'), ['class' => 'btn btn-circle green']) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
@endsection                                                
