@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{trans('strings.my_account')}}</div>

                <div class="panel-body">
                    {!! Form::model($user, ['route' => ['my_account.update', $user->id], 'method' => 'put', 'enctype'=>'multipart/form-data']) !!}
                        @include('app.my_account.form', ['button' => Lang::get('strings.edit') ])  
                    {!! Form::close() !!}

                    <br>
                    Facebook
                    <br>
                    @if($user->social_facebook)
                    Estás conectado como <code>{{$user->social_facebook->provider_user_email}}</code> con el id <code>{{$user->social_facebook->provider_user_id}}</code>
                    {!! Form::open(['url' => '/social_account/'.$user->social_facebook->social_account_id, 'method' => 'delete']) !!}
                        <button class="btn btn-danger" type="submit"><i class="fa fa-times-circle"></i> {{ Lang::get('strings.delete') }}</button>
                    {!! Form::close() !!}
                    @else
                    <a href="/auth/facebook" class="btn btn-block btn-social btn-facebook">
                      <span class="fa fa-facebook"></span>
                      Vincular con Facebook
                    </a>
                    @endif

                    Twitter
                    <br>
                    @if($user->social_twitter)
                    
                    {!! Form::open(['url' => '/social_account/'.$user->social_twitter->social_account_id, 'method' => 'delete']) !!}
                    Estás conectado como <code>{{$user->social_twitter->provider_user_email}}</code> con el id <code>{{$user->social_twitter->provider_user_id}}</code>
                        <button class="btn btn-danger" type="submit"><i class="fa fa-times-circle"></i> {{ Lang::get('strings.delete') }}</button>
                    {!! Form::close() !!}
                    @else
                    <a href="/auth/twitter" class="btn btn-block btn-social btn-twitter">
                      <span class="fa fa-twitter"></span>
                      Vincular con Twitter
                    </a>
                    @endif

                    Google
                    <br>
                    @if($user->social_google)
                    Estás conectado como <code>{{$user->social_google->provider_user_email}}</code> con el id <code>{{$user->social_google->provider_user_id}}</code>
                    {!! Form::open(['url' => '/social_account/'.$user->social_google->social_account_id, 'method' => 'delete']) !!}
                        <button class="btn btn-danger" type="submit"><i class="fa fa-times-circle"></i> {{ Lang::get('strings.delete') }}</button>
                    {!! Form::close() !!}
                    @else
                    <a href="/auth/google" class="btn btn-block btn-social btn-google">
                      <span class="fa fa-google"></span>
                      Vincular con Google
                    </a>
                    @endif

                    Github
                    <br>
                    @if($user->social_github)
                    Estás conectado como <code>{{$user->social_github->provider_user_email}}</code> con el id <code>{{$user->social_github->provider_user_id}}</code>
                    {!! Form::open(['url' => '/social_account/'.$user->social_github->social_account_id, 'method' => 'delete']) !!}
                        <button class="btn btn-danger" type="submit"><i class="fa fa-times-circle"></i> {{ Lang::get('strings.delete') }}</button>
                    {!! Form::close() !!}
                    @else
                    <a href="/auth/github" class="btn btn-block btn-social btn-github">
                      <span class="fa fa-github"></span>
                      Vincular con Github
                    </a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
