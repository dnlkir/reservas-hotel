@extends('metronic_layouts.partials.form2')

@section('form_body')

@section('title')
Reserva tu Habitación
@endsection	

<div class="form-body">
	<h3 class="form-section text-center">Reserva tu habitación</h3>
	<form class="horizontal-form" id="form_contacto" method="POST" action="{{ url('/reservation')}}">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Nombre Completo</label>
					{!! Form::text('name', null, ['required', 'class'=>'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Documento de identidad</label>
					{!! Form::number('identification_document', null, ['required', 'class'=>'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Teléfono</label>
					{!! Form::number('phone', null, ['required', 'class'=>'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Email</label>
					{!! Form::email('email', null, ['required', 'class'=>'form-control']) !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Cantidad de personas</label>
					{!! Form::number('amount_people', null, ['required', 'class'=>'form-control']) !!}
				</div>
			</div>
			
			<div class="col-md-6" style="z-index: 1">
				<label class="control-label">Fecha de reserva</label>
				<div class="input-group input-large date-picker input-daterange " data-date-format="yyyy/mm/dd" data-date-start-date="0d">
					{!! Form::text('entry_date', null, ['required', 'class'=>'form-control datepicker', 'autocomplete'=>'off']) !!}
					<span class="input-group-addon"> a </span>
					{!! Form::text('departure_date', null, ['required', 'class'=>'form-control datepicker', 'autocomplete'=>'off']) !!}
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label">Información de acompañantes</label>
					{!! Form::textarea('information_accompanist', null, ['required', 'class'=>'form-control', 'rows'=> '3']) !!}
				</div>
			</div>

		</div>
		<div class="text-center">
			<button type="submit" class="btn blue">
				<i class="fa fa-search"></i> Buscar
			</button>
		</div>


	</div>
</form>

{{-- FORMULARIO DE RESERVA --}}
<form class="horizontal-form" id="form_reserva" method="POST" action="{{ url('/save_reservation')}}">
	{{ csrf_field() }}
	<div class="container">

		{{-- {{$bedrooms}} --}}
		@php($count = 0)
		@if(isset($bedrooms))

		<h3 class="form-section text-center">Habitaciones Disponible</h3>
		
		@if(count($bedrooms) == 0)

		<div class="alert alert-info text-center">
			<strong>Atención!</strong> No se encuentran habitaciones para tu busqueda, intenta nuevamente.
		</div>

		@else
		<table class="table table-hover"> 
			<thead> 
				<tr> 
					<th>Ejegir</th> 
					<th>Habitación</th> 
					<th>Precio</th> 
					<th>Precio Total</th> 
					<th>Ver más</th> 
				</tr> 
			</thead> 
			<tbody>
				@foreach($bedrooms as $bedroom)

				@if(count($bedroom->reservations))

				@foreach($bedroom->reservations as $reservation)
				@if($reservation->status == "entregada")
				<tr> 
					<th scope="row">
						<div class="radio">
							<label>
								<input type="radio" name="bedroom_id" value="{{ $bedroom->id }}" checked>
							</label>
						</div>
					</th> 
					<td>{{ $bedroom->name }}</td> 
					<td>{{ $bedroom->price }}</td> 
					<td>{{ $bedroom->total_price }}</td> 
					<td>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$bedroom->id}}">
							Ver más
						</button>
					</td> 
				</tr> 

				<!-- Modal -->
				<div class="modal fade" id="myModal{{$bedroom->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Información de Habitación {{$bedroom->name}}</h4>
							</div>
							<div class="modal-body">
								<ul>
									<li>
										<strong>Piso:</strong>
										{{ $bedroom->floor->number }}
									</li>
									<li>
										<strong>Número de personas:</strong>
										{{ $bedroom->number_people }}
									</li>
									<li>
										<strong>Descripción:</strong>
										{{ $bedroom->description }}

									</li>
									<li>
										<strong>Precio:</strong>
										{{ $bedroom->price }}
									</li>
									<li>
										<strong>Servicios:</strong>
										@foreach($bedroom->services as $service)

										{{ $service->name }}, 

										@endforeach
									</li>
									<li>
										<strong>Foto:</strong><br>
										<div class="text-center">
											<img class="img-responsive" src="{{ $bedroom->picture }}" alt="">

										</div>
									</li>
								</ul>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>


				@endif
				@endforeach
				@else

				@php($count = $count + 1)

				<tr> 
					<th scope="row">
						<div class="radio">
							<label>
								<input type="radio" name="bedroom_id" value="{{ $bedroom->id }}" checked>
							</label>
						</div>
					</th> 
					<td>{{ $bedroom->name }}</td> 
					<td>{{ $bedroom->price }}</td> 
					<td>{{ $bedroom->total_price }}</td> 
					<td>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$bedroom->id}}">
							Ver más
						</button>
					</td> 
				</tr> 

				<!-- Modal -->
				<div class="modal fade" id="myModal{{$bedroom->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Información de Habitación {{$bedroom->name}}</h4>
							</div>
							<div class="modal-body">
								<ul>
									<li>
										<strong>Piso:</strong>
										{{ $bedroom->floor->number }}
									</li>
									<li>
										<strong>Número de personas:</strong>
										{{ $bedroom->number_people }}
									</li>
									<li>
										<strong>Descripción:</strong>
										{{ $bedroom->description }}

									</li>
									<li>
										<strong>Precio:</strong>
										{{ $bedroom->price }}
									</li>
									<li>
										<strong>Servicios:</strong>
										@foreach($bedroom->services as $service)

										{{ $service->name }}, 

										@endforeach
									</li>
									<li>
										<strong>Foto:</strong><br>
										<div class="text-center">
											<img class="img-responsive" src="{{ $bedroom->picture }}" alt="">

										</div>
									</li>
								</ul>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>

				@endif

				@endforeach 

			</tbody> 
		</table>

		{{-- COUNT --}}
		@if($count == 0)
		<div class="alert alert-info text-center">
			<strong>Atención!</strong> No se encuentran habitaciones para tu busqueda, intenta nuevamente.
		</div>
		
		@else
		<div class="form-actions text-center">
			<button id="reservar" type="submit" class="btn blue">
				<i class="fa fa-check"></i> Reservar
			</button>
		</div>
		@endif
		{{-- COUNT --}}


		@endif
	</div>

	<div class="row" style="display: none">
		{!! Form::text('name', null, ['required', 'class'=>'form-control']) !!}
		{!! Form::text('identification_document', null, ['required', 'class'=>'form-control']) !!}
		{!! Form::number('phone', null, ['required', 'class'=>'form-control']) !!}
		{!! Form::email('email', null, ['required', 'class'=>'form-control']) !!}
		{!! Form::text('entry_date', null, ['required', 'class'=>'form-control datepicker']) !!}
		{!! Form::text('departure_date', null, ['required', 'class'=>'form-control datepicker']) !!}
		{!! Form::number('amount_people', null, ['required', 'class'=>'form-control']) !!}
		{!! Form::text('information_accompanist', null, ['required', 'class'=>'form-control']) !!}
	</div>

	@endif


</form>
</div>




@endsection

@push('scripts')
<script src="/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$('#reservar').click(function(){
 	// alert('e');
 	$('#reservar').attr('disabled', 'disabled')
 	$('#form_reserva').submit();
 });
</script>
@endpush