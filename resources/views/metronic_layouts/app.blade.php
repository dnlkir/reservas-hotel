<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    @include('metronic_layouts.partials.head')
    <body class="page-header-fixed page-container-bg-solid page-content-white page-md page-sidebar-fixed page-footer-fixed">
        <div class="page-wrapper">
            @include('metronic_layouts.partials.header')
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"></div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                @include('metronic_layouts.partials.sidebar')
                @include('metronic_layouts.partials.content')
            </div>
            <!-- END CONTAINER -->
            @include('metronic_layouts.partials.footer')
        </div>
        <!--[if lt IE 9]>
        <script src="/metronic/global/plugins/respond.min.js"></script>
        <script src="/metronic/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        @include('metronic_layouts.partials.scripts')
        {!! Toastr::message() !!}
    </body>
</html>