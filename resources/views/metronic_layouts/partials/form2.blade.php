@extends('metronic_layouts.app2')

@push('styles')
<link href="/metronic/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
@endpush

@push('scripts')
<script src="/metronic/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/metronic_custom/pages/scripts/form-select2.js"></script>
<script src="/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="/metronic/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="/metronic/pages/scripts/components-multi-select.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="/metronic_custom/pages/scripts/form-input-mask.js"></script>
@endpush

@section('content')
									<div class="tab-content">
                                        <div class="tab-pane active" id="tab_0">
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-columns"></i>@yield('title')</div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                	@yield('form_body') 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
@endsection