<!-- BEGIN CORE PLUGINS -->
<script src="/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/metronic/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
@stack('scripts')
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="/metronic/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/metronic/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="/metronic/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<!-- BEGIN OTHER SCRIPTS -->
<script src="/metronic_custom/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<!-- END OTHER SCRIPTS -->