@role('admin')
{{-- <li class="heading">
    <h3 class="uppercase">{{trans('strings.manage')}}</h3>
</li> --}}
<li class="nav-item  ">
    <a href="{{ url('/admin/availability') }}" class="nav-link ">
        <i class="icon-calendar"></i>
        <span class="title">Disponibilidad</span>
    </a>
</li> 
<li class="nav-item  ">
    <a href="{{ url('/admin/users') }}" class="nav-link ">
        <i class="icon-user"></i>
        <span class="title">Usuarios</span>
    </a>
</li> 
<li class="nav-item  ">
    <a href="{{ url('/admin/floors') }}" class="nav-link ">
        <i class="icon-layers"></i>
        <span class="title">Pisos</span>
    </a>
</li> 
<li class="nav-item  ">
    <a href="{{ url('/admin/services') }}" class="nav-link ">
        <i class="icon-tag"></i>
        <span class="title">Servicios</span>
    </a>
</li> 
<li class="nav-item  ">
    <a href="{{ url('/admin/bedrooms') }}" class="nav-link ">
        <i class="icon-grid"></i>
        <span class="title">Habitaciones</span>
    </a>
</li> 
<li class="nav-item  ">
    <a href="{{ url('/admin/reservations') }}" class="nav-link ">
        <i class="icon-book-open"></i>
        <span class="title">Reservas</span>
    </a>
</li> 
<li class="nav-item  ">
    <a href="{{ url('/admin/historic') }}" class="nav-link ">
        <i class="icon-notebook"></i>
        <span class="title">Historial</span>
    </a>
</li>  
@endrole