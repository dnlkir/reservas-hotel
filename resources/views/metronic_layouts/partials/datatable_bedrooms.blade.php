@extends('metronic_layouts.app')

@section('title')
{{ $title }}
@endsection

@section('breadcrumb')
<li>
  <a href="{{ url('/home') }}">{{ trans('strings.home') }}</a>
  <i class="fa fa-circle"></i>
</li>
<li>
  <span>@yield('title')</span>
</li>
@endsection

@push('styles')
<link href="/metronic/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/metronic_custom/global/plugins/datatables/buttons/1.0.3/css/dataTables.buttons.min.css">
@endpush

@push('scripts')
<script src="/metronic/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="/metronic_custom/global/plugins/datatables/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered">
      <div class="form-body">
        <h2 class="text-center"><strong>DISPONIBILIDAD</strong></h2><br>
        @include('app.admin.availabilitys.form')
      </div>

      <div class="clearfix">

      </div>
      {{-- DATATABLE --}}
      <div class="portlet-title">
        <div class="caption font-dark">
          <i class="fa fa-table"></i>
          <span class="caption-subject bold uppercase">@yield('title')</span>
        </div>
        @if(!isset($HideMultipleDelete))
        <div class="actions">
          <div class="btn-group btn-group-devided" data-toggle="buttons">
            <a type="button" class="btn btn-transparent red btn-outline btn-circle btn-sm active delete_all">
              <i class="fa fa-trash"/></i>{{trans('strings.delete_selected')}}
            </a>
          </div>
        </div>
        @endif
      </div>
      <div class="portlet-body">
        @if(!isset($HideCreate))
        <div class="table-toolbar">
          <div class="row">
            <div class="col-md-6">
              <div class="btn-group">
                <a class="btn sbold green" href="{{ url(sprintf('/%s/create', \Request::path())) }}">
                  <i class="fa fa-plus"/></i>{{trans('strings.create')}}
                </a>
              </div>
            </div>
          </div>
        </div>
        @endif
        {!! $dataTable->table(['class' => 'table table-striped table-bordered table-hover dt-responsive'], true) !!}
      </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
  </div>
</div>
@endsection