<?php

return [    
    
    'copyright'     => date('Y') .' &copy; ' . config('app.name'),
    'rights_reserved' => 'All rights reserved.',
    'developed_by'  => 'Developed by',
    'developer'     => 'DesarrollAPPmos',
    'developer_url' => 'https://desarrollappmos.com',

    'id'                => 'ID',
    'name'              => 'Name',
    'username'          => 'Username',
    'picture'           => 'Picture',
    'description'       => 'Description',
    'active'            => 'Active',
    'inactive'          => 'Inactive',
    'status'            => 'Status',
    'tools'             => 'Tools',
    'create'            => 'Create',
    'edit'              => 'Edit',
    'create_table'      => 'Create :table',
    'edit_table'        => 'Edit :table',
    'delete'            => 'Delete',
    'delete_selected'   => 'Delete selected',
    'save'              => 'Save',
    'connect'           => 'Connect',
    'language'          => 'Language',
    'display_name'      => 'Display name',
    'display_picture'   => 'Display picture',
    'profile_picture'   => 'Profile picture',
    'code'              => 'Code',
    'translation'       => 'Translation',
    'translations'      => 'Translations',
    'email_or_username' => 'Email or Username',

    'home'          => 'Home',
    'manage'        => 'Manage',
    'form'          => 'Form',
    'table'         => 'Table',
    'select_image'  => 'Select an image',
    'change_image'  => 'Change image',
    'remove_image'  => 'Remove image',
    'submit'        => 'Submit',
    'back'          => 'Back',
    'all'           => 'All',
    'my_account'    => 'My account',
    'back_web'      => 'Back to web page',
    'logged_in'     => 'You are logged in!',

    'personal_info'     => 'Personal information',
    'change_password'   => 'Change password',

    'login'                     => 'Login',
    'login_label'               => 'Login to your account',
    'logout'                    => 'Logout',
    'dont_have_account'         => 'Don\'t have an account yet?',
    'register'                  => 'Sign Up',
    'register_label'            => 'Enter your personal details below:',
    'email'                     => 'Email',
    'password'                  => 'Password',
    'password_confirmation'     => 'Re-type Your Password',
    'password_forgot'           => 'Forgot your password?',
    'password_forgot_label'     => 'Enter your e-mail address below to reset your password. You will receive an email with the instructions.',
    'password_current'          => 'Current password',
    'password_current_error'    => 'Current password do not match',
    'remember_me'               => 'Remember me',
    'reset_password'            => 'Reset Password',
    'reset_password_label'      => 'Enter your e-mail address and your new password.',
    'account_updated'           => 'Your account information has been updated',  
    'or_login_with'             => 'Or login with',
    'fcm_token'                 => 'Fcm Token',

    'yes'       => 'Yes',
    'no'        => 'No',
    'copy'      => 'Copy',
    'print'     => 'Print',
    'reset'     => 'Reset',
    'export'    => 'Export',

    'locale' => 'Locale',

    'confirm_delete'            => 'Are you sure you want to delete = :variable?',
    'confirm_delete_multiple'   => 'Are you sure you want to delete the selected rows?',
    'delete_nothing_selected'   => 'Nothing selected',

    'created'        => '":variable" was created successfully',
    'updated'        => '":variable" was updated successfully',
    'deleted'        => '":variable" was deleted successfully',
    'deleted_plural' => '":variable" were deleted successfully',

    'social_accounts' => 'Social accounts',

    'audit'     => 'Audit',
    'audits'    => 'Audits',

    'type'              => 'Type',
    'auditable_id'      => 'Auditable Id',
    'auditable_type'    => 'Model',
    'old'               => 'Old',
    'new'               => 'New',
    'route'             => 'Route',
    'ip_address'        => 'IP Address',

    'role'          => 'Role',
    'roles'         => 'Roles',
    'role_select'   => 'Select a role',

    'user'          => 'User',
    'users'         => 'Users',
    'user_name'     => 'User name',
    'user_select'   => 'Select an user',

    'role_tr'        => 'Role translation',
    'role_trs'       => 'Role translations',

    'request_logs'      => 'Request Logs',
    'path'              => 'Path',
    'country'           => 'Country',
    'client'            => 'Client',
    'client_app_ver'    => 'Client App Version',

];
