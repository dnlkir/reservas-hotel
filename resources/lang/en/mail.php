<?php

return [

    'hello'                     => 'Hello!',
    'regards'                   => 'Regards',
    'trouble_clicking_button'   => 'If you’re having trouble clicking the ":variable" button, copy and paste the URL below into your web browser:',

    //Reset Password
    'reset_password_subject'    => 'Reset Password',
    'reset_password_action'     => 'Reset',
    'reset_password_line_1'     => 'You are receiving this email because we received a password reset request for your account.',
    'reset_password_line_2'     => 'If you did not request a password reset, no further action is required.',

];
