<?php

return [
	
    'messages'	=> [
    	'done'	=> 'Done',
        'user_updated_successfully' => 'Your account information has been updated',
        'profile_picture_updated_successfully'  => 'Profile picture has been updated succesfully',
    ],

    'errors' => [
    	'invalid_credentials'		=> 'Invalid credentials',
    	'could_not_create_token'	=> 'Couldn\'t create token', 
        'permission_denied'     	=> 'Permission denied',
        'not_found'             	=> 'Not found',
        'image_not_selected'        => 'You have not selected an image',
    ],    

];
