<?php

return [

	'hello'                     => 'Hola!',
	'regards'                   => 'Saludos',
	'trouble_clicking_button'   => 'Si estás teniendo problemas al darle click al boton ":variable", copia y pega la siguiente URL en tu navegador:',

    //Reset Password
    'reset_password_subject' 	=> 'Recuperar contraseña',
    'reset_password_action'     => 'Recuperar',
    'reset_password_line_1'     => 'Estás recibiendo este correo porque hemos recibido una solicitud de recuperación de contraseña para tu cuenta.',
    'reset_password_line_2'     => 'Si no hiciste la solicitud, ignora este mensaje.',

];
