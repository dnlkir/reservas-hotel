<?php

return [
	
    'messages'	=> [
    	'done'	=> 'Hecho!',
        'user_updated_successfully' => 'La información de tu cuenta ha sido actualizada',
        'profile_picture_updated_successfully'  => 'Tu foto de perfil ha sido actualizada',
    ],

    'errors' => [
    	'invalid_credentials'		=> 'Credenciales inválidas',
    	'could_not_create_token'	=> 'No es posible crear el token', 
        'permission_denied'     	=> 'Permiso denegado',
        'not_found'             	=> 'No encontrado',
        'image_not_selected'        => 'No has seleccionado ninguna imagen',
    ],    

];
