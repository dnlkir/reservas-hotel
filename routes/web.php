<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('/auth/login');
// });

Route::get('/', function () {
    return view('/app/user/form');
});

Auth::routes();

// Route::get('/register', function () {
//     return view('/auth/login');
// });

Route::get('/home', 'HomeController@index')->name('home');

Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'Web\LanguageController@switchLang']);

Route::get('/my_account', 'Web\MyAccountController@index');
Route::put('/my_account/{user}', ['as'=>'my_account.update', 'uses'=>'Web\MyAccountController@update']);

Route::post('reservation', 'Web\WebController@reservation');
Route::post('save_reservation', 'Web\WebController@save_reservation');


Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth' ,'role:admin']], function () {
	
	Route::resource('users', 'Web\Admin\UserController');
	Route::post('users/delete_multiple', 'Web\Admin\UserController@deleteMultiple');

	Route::resource('floors', 'Web\Admin\FloorController');
	Route::post('floors/delete_multiple', 'Web\Admin\FloorController@deleteMultiple');

	Route::resource('services', 'Web\Admin\ServiceController');
	Route::post('services/delete_multiple', 'Web\Admin\ServiceController@deleteMultiple');

	Route::resource('bedrooms', 'Web\Admin\BedroomController');
	Route::post('bedrooms/delete_multiple', 'Web\Admin\BedroomController@deleteMultiple');

	Route::resource('reservations', 'Web\Admin\ReservationController');
	Route::post('reservations/delete_multiple', 'Web\Admin\ReservationController@deleteMultiple');

	Route::get('reservations/{bedrooms}/create_reservation', 'Web\Admin\ReservationController@create_reservation');

	Route::resource('historic', 'Web\Admin\HistoricController');

	Route::resource('availability', 'Web\Admin\AvailabilityController');
	Route::post('availability/delete_multiple', 'Web\Admin\AvailabilityController@deleteMultiple');

});
