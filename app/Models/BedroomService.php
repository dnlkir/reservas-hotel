<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BedroomService extends Model
{
	protected $table = 'bedroom_services';
    protected $fillable = ['bedroom_id', 'service_id'];

    public function bedroom(){
        return $this->belongsToMany('App\Models\Bedroom');
    }
}
