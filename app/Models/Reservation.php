<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['bedroom_id','name','identification_document','amount_people','phone','email','status','entry_date','departure_date', 'total_price','information_accompanist'];

    public function bedroom(){
        return $this->belongsTo('App\Models\Bedroom');
    }

    public function bedroom_status(){
        return $this->belongsTo('App\Models\Bedroom');
    }


}
