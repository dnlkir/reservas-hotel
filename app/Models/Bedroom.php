<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bedroom extends Model
{
	protected $fillable = ['floor_id', 'number_people', 'name', 'description', 'price', 'picture'];
	
	protected $filesystem = 'pictures';


	public function floor(){
		return $this->belongsTo('App\Models\floor');
	}

	public function services(){
        return $this->belongsToMany('App\Models\Service');
    }

    public function reservations(){
		return $this->hasMany('App\Models\Reservation');
	}

	public function reservations_search(){
		return $this->hasMany('App\Models\Reservation')->where('status', '!=', 'entregada');
	}

	public function reservations_availability(){
		return $this->HasMany('App\Models\Reservation')->whereStatus('ocupada')->orWhere('status', '=', 'reserva')->orderBy('id', 'asc');
	}

	public function reservations_availability2(){
		return $this->HasOne('App\Models\Reservation')->whereStatus('ocupada')->orWhere('status', '=', 'reserva')->orderBy('id', 'asc');
	}

	public function deleteCurrentImage($image){
		$actual = $image;
		$file_name = substr($actual, -24); 
		\Storage::disk($this->filesystem)->delete($file_name);
	}

	public function uploadPicture($file,$column,$format,$resize,$resize_x,$resize_y){

		$path = \Config::get('filesystems.disks.'.$this->filesystem.'.path');  
		$image_name = str_random(20).'.'.$format.'';
		\Storage::disk($this->filesystem)->put($image_name,  \File::get($file));

		$image = \Image::make(sprintf('%s/%s', $path, $image_name));

		if($resize == '1'){
			$image->crop($resize_x, $resize_y);
		}
		$image->save();   

		$this->$column = (sprintf('/%s/%s', $path, $image_name));
		$this->save();  
	}

}
