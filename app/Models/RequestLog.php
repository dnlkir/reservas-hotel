<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    protected $fillable = [
    	'user_id', 
        'path',
        'country', 
        'language',
        'client',
        'client_app_ver',
        'ip_address'
    ];
}
