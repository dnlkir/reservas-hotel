<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Notifications\CustomResetPassword;

class User extends Authenticatable implements UserResolver
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'username', 'email', 'picture', 'password', 'language'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token){
        $this->notify(new CustomResetPassword($token));
    }

    /**
     * {@inheritdoc}
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = ucwords(mb_strtolower($value));
    }

    public function setEmailAttribute($value) {
        $this->attributes['email'] = mb_strtolower($value);
    }

    public function setUsernameAttribute($value) {
        $this->attributes['username'] = mb_strtolower($value);
    }

    public static function boot()
    {
        parent::boot();
        
        User::deleting(function($user)
        {   
            $user->deleteCurrentImage();

        });
    }

    protected $filesystem = 'pictures';

    public function deleteCurrentImage(){
        $actual = $this->picture;
        $file_name = substr($actual, -24); 
        \Storage::disk($this->filesystem)->delete($file_name);
    }

    public function uploadPicture($file){
        $path = \Config::get('filesystems.disks.'.$this->filesystem.'.path');  

        $this->deleteCurrentImage();

        $image_name = str_random(20).'.jpg';
        \Storage::disk($this->filesystem)->put($image_name,  \File::get($file));

        $image = \Image::make(sprintf('%s/%s', $path, $image_name));
        $image->fit(intval(300), null, function($constraint) {
             $constraint->aspectRatio();
        });
        $image->save();   
         
        $this->picture = (sprintf('%s/%s/%s', env('APP_URL'), $path, $image_name));
        $this->save();  
    }
}
