<?php

namespace App\DataTables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomDtConfig extends Controller
{
    public static function addLanguageFlag($country_code, $translation = null){
        $flag_url = sprintf('%s/%s.png', url('/metronic_custom/language_flags'), $country_code);
        $country_translation = config('languages.'.$country_code);
        $img = '<img src="'.$flag_url.'">';
        if(isset($translation)){
            return sprintf('%s %s - %s. ', $img, $country_code, $translation);
        }else{
            return sprintf('%s %s - %s. ', $img, $country_code, $country_translation);
        }
    }

    public static function addCountryFlag($country_code){
        $flag_url = sprintf('%s/%s.png', url('/metronic/global/img/flags'), $country_code);
        $country_translation = trans('countries.'.$country_code);
        $img = '<img src="'.$flag_url.'">';
        return sprintf('%s %s - %s. ', $img, $country_code, $country_translation);
    }

    public static function addPicture($link){
        if($link == ""){
            return '<img width="100" src="/img/profile-placeholder.jpg">';
        }else{
            return '<a target="_blank" href="'.$link.'"><img width="100" src="'.$link.'"></a>';
        }
    }

    public static function customActions($id, $name, $hasEdit = true, $hasDelete = true){
        $confirm_delete = "return confirm('".trans('strings.confirm_delete', ['variable' => $name])."');return false;";
        $url_base =sprintf('/%s', \Request::path()); 
        $url_edit = url(sprintf("%s/%s/edit",$url_base, $id)); 
        $url_delete = url(sprintf("%s/%s",$url_base, $id)); 

        $buttonEdit = $hasEdit ? '<a href="'.$url_edit.'"class="btn btn-icon-only blue"><i class="fa fa-edit"></i></a>' : '';
        $buttonDelete = $hasDelete ? '<button type="submit"onclick="'.$confirm_delete.'"form="delete_form_'.$id.'"class="btn btn-icon-only red"><i class="fa fa-trash"></i></button>' : '';
        $formDelete = $hasDelete ? '<form id="delete_form_'.$id.'" action="'.$url_delete.'" method="POST"> <input type="hidden" name="_method" value="DELETE"> <input type="hidden" name="_token" value="'.csrf_token().'"> </form>' : '';

        return $formDelete.$buttonEdit.$buttonDelete;
    }

    public static function checkboxSingle($id){
      return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input type="checkbox" class="checkboxes" data-id="'.$id.'"> <span></span> </label>';
  }

  public static function actionConfig(){
      return ['class' => 'all', 'title' => trans('strings.tools'), 'printable' => false, 'exportable' => false, 'searchable' => false, 'filterable' => false];
  }

  public static function customParameters($filterAll = false){
    $custom_parameters = ([
        "dom" => "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        'buttons' => self::buttons(),
        'language' => [
            'url' => url('/metronic_custom/global/plugins/datatables/locales/'. \App::getLocale(). '.json'),
        ],
        'initComplete' => "function () {                            
            ".self::filterInAllColumns($filterAll).self::checkBoxScript()."
        }",

    ]);
    return $custom_parameters;
}

public static function filterInAllColumns($check){
    $script = "this.api().columns().every(function(){var column=this;var input=document.createElement(\"input\");$(input).appendTo($(column.footer()).empty()).on('change',function(){column.search($(this).val(),false,false,true).draw()})});";
    return $check ? $script : '';
}

public static function checkBoxHtml(){
  $checkbox_html = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox"class="group-checkable"data-set="#dataTableBuilder .checkboxes"><span></span></label>';
  $config = ['class' => 'all', 'title' => $checkbox_html, 'printable' => false, 'exportable' => false, 'searchable' => false, 'filterable' => false];
  return $config;
}

public static function checkBoxScript(){
    $url_base = sprintf('/%s/', \Request::path());

    $confirm_delete_selected = trans('strings.confirm_delete_selected');
    $delete_nothing_selected = trans('strings.delete_nothing_selected');

    $checkbox_script = 'var table=$("#dataTableBuilder");table.find(".group-checkable").change(function(){var a=jQuery(this).attr("data-set"),b=jQuery(this).is(":checked");jQuery(a).each(function(){b?($(this).prop("checked",!0),$(this).parents("tr").addClass("active")):($(this).prop("checked",!1),$(this).parents("tr").removeClass("active"))})}),table.on("change","tbody tr .checkboxes",function(){$(this).parents("tr").toggleClass("active")});jQuery(".delete_all").on("click",function(e){var b=[];$(".checkboxes:checked").each(function(){b.push($(this).attr("data-id"))});if(b.length<=0){alert("'.$delete_nothing_selected.'")}else{WRN_PROFILE_DELETE="'.$confirm_delete_selected.'";var c=confirm(WRN_PROFILE_DELETE);if(c==true){var d="'.csrf_token().'";var f=b;$.ajax({type:"post",url:"'.$url_base.'"+"delete_multiple",headers:{"X-CSRF-TOKEN":"'.csrf_token().'"},data:{"id":f,"_method":"post"},success:function(a){location.reload()}})}}});';

    return $checkbox_script;
}

public static function buttons(){
    $buttons = ([    
        [
            'extend'    => 'collection',
            'text'      => '<i class="fa fa-download"></i>&nbsp;'.trans('strings.export').'</i><span class="caret"></span>',
            'className' => 'btn green btn-outline',
            'buttons'   => [
                [
                    'extend'    => 'excel', 
                    'text'      => '<i class="fa fa-file-excel-o">&nbsp;Excel</i>',
                ],
                // [
                //     'extend'    => 'csv', 
                //     'text'      => '<i class="fa fa-file-excel-o">&nbsp;CSV</i>',
                //     "sCharSet" => "utf16le"
                // ], 
            ],
        ],                                 
        [
            'extend'    => 'copy', 
            'text'      => '<i class="fa fa-files-o">&nbsp;'.trans('strings.copy').'</i>',
            'className' => 'btn green btn-outline',
        ],

        [
            'extend'    => 'print', 
            'text'      => '<i class="fa fa-print">&nbsp;'.trans('strings.print').'</i>',
            'className' => 'btn green btn-outline',
        ],
        [
            'extend'    => 'reset', 
            'text'      => '<i class="fa fa-undo">&nbsp;'.trans('strings.reset').'</i>',
            'className' => 'btn green btn-outline',
        ], 
    ]);
    return $buttons;
}
}
