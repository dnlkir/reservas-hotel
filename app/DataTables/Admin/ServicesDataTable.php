<?php

namespace App\DataTables\Admin;

use App\Models\Service as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class ServicesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('checkbox', function ($model) {
            return CustomDtConfig::checkboxSingle($model->getKey());
        })
        ->addColumn('action', function ($model) {
            return CustomDtConfig::customActions($model->getKey(), $model->name);
        })
        ->escapeColumns([])
        ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Model $model)
    {
        return $model->newQuery()->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->addAction(CustomDtConfig::actionConfig()) 
        ->addCheckBox(CustomDtConfig::checkBoxHtml())
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
        ['data'=>'id','name'=>'id','title'=>'Id','class'=>'hidden'],
        ['data'=>'name','name'=>'name','title'=>'Nombre','class'=>'all'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admin_servicesdatatables_' . time();
    }
}
