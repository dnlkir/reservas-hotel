<?php

namespace App\DataTables\Admin;

use App\Models\Bedroom as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class BedroomsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('services', function ($model) {
            return $model->services->map(function($service) {
                return sprintf("%s. ", $service->name);
            })->implode('<br>');                           
        })
        ->addColumn('picture', function ($model) {
            return CustomDtConfig::addPicture($model->picture);
        })
        ->addColumn('checkbox', function ($model) {
            return CustomDtConfig::checkboxSingle($model->getKey());
        })
        ->addColumn('action', function ($model) {
            return CustomDtConfig::customActions($model->getKey(), $model->name);
        })
        ->escapeColumns([])
        ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Model $model)
    {
        return $model->newQuery()->with('floor')->with('services')->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->addAction(CustomDtConfig::actionConfig()) 
        ->addCheckBox(CustomDtConfig::checkBoxHtml())
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
        ['data'=>'picture','name'=>'picture','title'=>'Foto','class'=>'all','orderable'=>false,'exportable'=>false,'printable'=>false],
        ['data'=>'id','name'=>'id','title'=>'Id','class'=>'hidden'],
        ['data'=>'floor.number','name'=>'floor.number','title'=>'Piso','class'=>'all'],
        ['data'=>'number_people','name'=>'number_people','title'=>'Número de personas','class'=>'all'],
        ['data'=>'name','name'=>'name','title'=>'Nombre','class'=>'all'],
        ['data'=>'description','name'=>'description','title'=>'Descripción','class'=>'all'],
        ['data'=>'price','name'=>'price','title'=>'Precio','class'=>'all'],
        ['data'=>'services','name'=>'services.name','title'=>'Servicios','class'=>'all'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admin_bedroomsdatatables_' . time();
    }
}
