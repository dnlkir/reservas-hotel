<?php

namespace App\DataTables\Admin;

use App\Role as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class RoleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('translations', function ($model) {
            $url_base = sprintf('/%s/', \Request::path());
            $link = sprintf('%s%s/role_trs', $url_base, $model->id);
            $element = '<a href="'.$link.'" class="btn btn-icon-only blue"><i class="fa fa-eye"></i></a>';

            $map = $model->translations->map(function($role_tr) {
                return CustomDtConfig::addLanguageFlag($role_tr->locale, $role_tr->display_name);
            })->implode('<br>');  

            return $map.'<br>'.$element;                           
        })
        ->addColumn('checkbox', function ($model) {
            return CustomDtConfig::checkboxSingle($model->getKey());
        })
        ->addColumn('action', function ($model) {
            return CustomDtConfig::customActions($model->getKey(), $model->name);
        })
        ->escapeColumns([])
        ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Model $model)
    {
        return $model->newQuery()->with('translations')->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->addAction(CustomDtConfig::actionConfig()) 
        ->addCheckBox(CustomDtConfig::checkBoxHtml())
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
        ['data'=>'id','name'=>'id','title'=>trans('strings.id'),'class'=>'all'],
        ['data'=>'name','name'=>'name','title'=>trans('strings.name'),'class'=>'all'],
        ['data'=>'translations','name'=>'translations.display_name','title'=>trans('strings.translations'),'class'=>'all'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admin_roledatatables_' . time();
    }
}
