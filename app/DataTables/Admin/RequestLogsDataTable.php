<?php

namespace App\DataTables\Admin;

use App\Models\RequestLog as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class RequestLogsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->escapeColumns([])
            ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Model $model)
    {
        return $model->newQuery()->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=>'id','name'=>'id','title'=>trans('strings.id'),'class'=>'all'],
            ['data'=>'user_id','name'=>'user_id','title'=>trans('strings.user'),'class'=>'all'],
            ['data'=>'path','name'=>'path','title'=>trans('strings.path'),'class'=>'all'],
            ['data'=>'country','name'=>'country','title'=>trans('strings.country'),'class'=>'all'],
            ['data'=>'language','name'=>'language','title'=>trans('strings.language'),'class'=>'all'],
            ['data'=>'client','name'=>'client','title'=>trans('strings.client'),'class'=>'all'],
            ['data'=>'client_app_ver','name'=>'client_app_ver','title'=>trans('strings.client_app_ver'),'class'=>'all'],
            ['data'=>'ip_address','name'=>'ip_address','title'=>trans('strings.ip_address'),'class'=>'all'],    
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AdminRequestLogsDataTable_' . time();
    }
}
