<?php

namespace App\DataTables\Admin;

use App\User as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('roles', function ($model) {
            return $model->roles->map(function($role) {
                return sprintf("%s. ", $role->name);
            })->implode('<br>');
        })
        ->addColumn('picture', function ($model) {
            return CustomDtConfig::addPicture($model->picture);
        })
        ->addColumn('checkbox', function ($model) {
            return CustomDtConfig::checkboxSingle($model->getKey());
        })
        ->addColumn('action', function ($model) {
            return CustomDtConfig::customActions($model->getKey(), $model->name);
        })
        ->escapeColumns([]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Model $model)
    {
        return $model->newQuery()->with('roles')->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->addAction(CustomDtConfig::actionConfig()) 
        ->addCheckBox(CustomDtConfig::checkBoxHtml())
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
        ['data'=>'picture','name'=>'picture','title'=>'Foto','class'=>'all','orderable'=>false,'exportable'=>false,'printable'=>false],
        ['data'=>'id','name'=>'id','title'=>'Id','class'=>'hidden'],
        ['data'=>'name','name'=>'name','title'=>'Nombre','class'=>'all'],
        ['data'=>'username','name'=>'username','title'=>'Nombre de usuario','class'=>'all'],
        ['data'=>'email','name'=>'email','title'=>'Email','class'=>'all'],
        ['data'=>'roles','name'=>'roles.name','title'=>'Roles','class'=>'all'],            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admin_usersdatatable_' . time();
    }
}
