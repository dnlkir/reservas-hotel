<?php

namespace App\DataTables\Admin;

use App\Models\Reservation as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class ReservationsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('checkbox', function ($model) {
            return CustomDtConfig::checkboxSingle($model->getKey());
        })
        ->addColumn('action', function ($model) {
            return CustomDtConfig::customActions($model->getKey(), $model->name);
        })
        ->escapeColumns([])
        ;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Model $model)
    {
        // return $model->newQuery()
        // ->with('bedroom')
        // ->where('status', 'reserva')
        // ->orWhere('status', 'ocupada')
        // ->orderBy('id', 'desc');

        return $model->newQuery()
        ->with('bedroom')
        ->where(function ($query) {
            $query->where('status', '=', 'reserva')
            ->orWhere('status', '=', 'ocupada');
        })
        ->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->addAction(CustomDtConfig::actionConfig()) 
        ->addCheckBox(CustomDtConfig::checkBoxHtml())
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data'=>'id','name'=>'id','title'=>'Id','class'=>'hidden'],
            ['data'=>'bedroom.name','name'=>'bedroom.name','title'=>'Habitación','class'=>'all'],
            ['data'=>'name','name'=>'name','title'=>'Nombre','class'=>'all'],
            ['data'=>'identification_document','name'=>'identification_document','title'=>'Documento de identidad','class'=>'all'],
            ['data'=>'amount_people','name'=>'amount_people','title'=>'Cantidad de personas','class'=>'all'],
            ['data'=>'phone','name'=>'phone','title'=>'Teléfono','class'=>'all'],
            ['data'=>'email','name'=>'email','title'=>'email','class'=>'all'],
            ['data'=>'entry_date','name'=>'entry_date','title'=>'Fecha de entrada','class'=>'all'],
            ['data'=>'departure_date','name'=>'departure_date','title'=>'Fecha de salida','class'=>'all'],
            ['data'=>'total_price','name'=>'total_price','title'=>'Precio total','class'=>'all'],
            ['data'=>'information_accompanist','name'=>'information_accompanist','title'=>'Información de acompañante','class'=>'all'],
            ['data'=>'status','name'=>'status','title'=>'Estado','class'=>'all'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admin_reservationsdatatables_' . time();
    }
}
