<?php

namespace App\DataTables\Admin;

use App\Models\Audit as Model;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\DataTables\CustomDtConfig;

class AuditDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->escapeColumns([]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Model $model)
    {
        return $model->newQuery()->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters(CustomDtConfig::customParameters())
        ;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            /*['data'=>'type','name'=>'type','title'=>trans('strings.type'),'class'=>'all'],
            ['data'=>'auditable_id','name'=>'auditable_id','title'=>trans('strings.auditable_id'),'class'=>'all'],
            ['data'=>'auditable_type','name'=>'auditable_type','title'=>trans('strings.auditable_type'),'class'=>'all'],
            ['data'=>'old','name'=>'old','title'=>trans('strings.old'),'class'=>'none'],
            ['data'=>'new','name'=>'new','title'=>trans('strings.new'),'class'=>'none'],
            ['data'=>'user_id','name'=>'user_id','title'=>trans('strings.user'),'class'=>'all'],
            ['data'=>'route','name'=>'route','title'=>trans('strings.route'),'class'=>'all'],
            ['data'=>'ip_address','name'=>'ip_address','title'=>trans('strings.ip_address'),'class'=>'all'], */
            'id','user_id','event','auditable_type','auditable_id','old_values','new_values','url','ip_address','user_agent' 
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'admin_auditdatatables_' . time();
    }
}
