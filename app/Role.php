<?php

namespace App;

use Laratrust\LaratrustRole;
use \Dimsav\Translatable\Translatable;

class Role extends LaratrustRole
{
    use Translatable;

    protected $primaryKey = 'id';

    public $translatedAttributes = ['display_name', 'description'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function boot()
    {
        parent::boot();
        
        Role::deleting(function($role)
        {   
            $role->translations->each->delete();
        });
    }
}
