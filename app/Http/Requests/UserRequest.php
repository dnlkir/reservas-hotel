<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $email_rule     = 'required|email|max:255|unique:users,email,'.$this->get('id');
            $username_rule  = 'required|max:20|alpha_dash|unique:users,username,'.$this->get('id');
        }else{
            $email_rule     = 'required|email|max:255|unique:users';
            $username_rule  = 'required|max:20|alpha_dash|unique:users';
        }

        $password_rule = !empty($this->get('password')) ? 'sometimes|required|confirmed|min:6' : 'sometimes';

        return [
            'name'          => 'required|min:5|max:255',
            'email'         => $email_rule,
            'username'      => $username_rule,
            'password'      => $password_rule,
        ];
    }

}
