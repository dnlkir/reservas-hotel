<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleTrRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT'){
            $role_id_rule = 'required|not_in:0|unique_with:role_trs,locale,'.$this->get('id');
        }else{
            $role_id_rule = 'required|not_in:0|unique_with:role_trs,locale';
        }
        return [
            'role_id' => $role_id_rule,
            'display_name' => 'required|max:30|min:3',
            'description' => 'required|min:3|max:100',
        ];
    }
}
