<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Floor;
use App\DataTables\Admin\FloorsDataTable as DataTable;

class FloorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        // TITULO A MOSTRAR EN VISTA
        $title = 'Pisos';
        // RETORNAMOS A LA VISTA
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //INSTANCIAMOS EL MODELO PARA DEVOLVERLO A LA VISTA
        $floor = new Floor();
        // RETORNAMOS A LA VISTA
        return view('app.admin.floors.preform', compact('floor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // VALIDACION DE LA SOLICITUD DESDE FORMULARIO
    	$request->validate([
    		'number' => 'required|numeric'
    	]);

        //CREAMOS LA SOLICITUD EN LA BASE DE DATOS
        $floor = Floor::create($request->all());
        //CREAMOS EL MENSAJE
        \Toastr::success(trans('strings.created', ['variable' => $floor->number]));
        // RETORNAMOS A LA VISTA
        return redirect('admin/floors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Floor $floor)
    {
        // RETORNAMOS A LA VISTA EL MODELO
        return $floor;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Floor $floor)
    {
        // RETORNAMOS A LA VISTA
        return view('app.admin.floors.preform', compact('floor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Floor $floor)
    {
        // VALIDACION DE LA SOLICITUD DESDE FORMULARIO
    	$request->validate([
    		'number' => 'required|numeric'
    	]);

        //ACTUALIZAMOS LA SOLICITUD EN LA BASE DE DATOS
        $floor->update($request->all());
        //CREAMOS EL MENSAJE
        \Toastr::success(trans('strings.updated', ['variable' => $floor->number]));
        // RETORNAMOS A LA VISTA
        return redirect('admin/floors/');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Floor $floor)
    {
        //ELIMINAMOS LA SOLICITUD EN LA BASE DE DATOS
        $floor->delete();
        //CREAMOS EL MENSAJE
        \Toastr::success(trans('strings.deleted', ['variable' => $floor->number]));
        // RETORNAMOS A LA VISTA
        return redirect()->to('/admin/floors');
    }

    public function deleteMultiple(Request $request)
    {   
        // FUNCION PARA ELIMINAR MULTIPLE SOLICITUD DE CHECKBOX
        // BUSCAMOS EL REGISTRO A ELIMINAR
        $floors = Floor::find($request['id']);
        // CONTAMOS CUANTOS REGISTROS HAY POR ELIMINAR
        $count = Floor::find($request['id'])->count();
        // ELIMINAMOS LOS REGISTROS
        Floor::destroy($request['id']);
        // VARIABLE DE MENSAJE
        $names = "";
        // RECORREMOS LOS REGISTROS PARA GUARDAR EL NUMERO O NOMBRE A MOSTRAR EN MENSAJE
        foreach ($floors as $floor) {
            if($names==""){
                $names = $floor->number;
            }else{
                $names = $names . ", " . $floor->number;
            }
        }
        // MOSTRAMOS EL MENSAJE SEGUN LA CANTIDAD DE REGISTROS A ELIMINAR
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
