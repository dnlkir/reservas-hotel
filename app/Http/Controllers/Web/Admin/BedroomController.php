<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bedroom;
use App\Models\Floor;
use App\Models\Service;
use App\Models\BedroomService;
use App\Models\Reservation;
use DateTime;
use App\DataTables\Admin\BedroomsDataTable as DataTable;

class BedroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        // TITULO A MOSTRAR EN VISTA
    	$title = 'Habitaciones';
        //CONSULTO LAS HABITACIONES CON LAS RESERVAS
        $bedrooms = Bedroom::with(['reservations_availability' => function ($query) {
            $date = new DateTime('now');
            $date = $date->format('Y-m-d');
            $query->where('entry_date', '<=', $date);
            $query->where('departure_date', '>=', $date);

        }])->get();
        
        // RETORNAMOS A LA VISTA
    	return $dataTable->render('metronic_layouts.partials.datatable_bedrooms', compact('title','bedrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //INSTANCIAMOS EL MODELO PARA DEVOLVERLO A LA VISTA
    	$bedroom = new Bedroom();
        // TOMAMOS LOS PISOS DE LA BASE DE DATOS PAR MOSTRAR EN UN SELECT
    	$floors = Floor::all()->pluck('number', 'id');
        // TOMAMOS LOS SERVICIOS DE LA BASE DE DATOS PAR MOSTRAR EN UN SELECT
        $services = Service::all()->pluck('name', 'id');
        // RETORNAMOS A LA VISTA
    	return view('app.admin.bedrooms.preform', compact('bedroom','floors', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // VALIDACION DE LA SOLICITUD DESDE FORMULARIO
    	$request->validate([
            'floor_id' => 'required',
    		'number_people' => 'required',
    		'name' => 'required',
    		'description' => 'required',
    		'price' => 'required',
    		'picture' => 'required'
    	]);
        //CREAMOS LA SOLICITUD EN LA BASE DE DATOS
    	$bedroom = Bedroom::create($request->all());

        // SI LA SOLICITUD TRAE SERVICIOS LOS GUARDAMOS EN LA TABLA MUCHOS A MUCHOS
    	$service_id_array = $request->input('service_id');
        if($service_id_array!=null){
            $bedroom->services()->attach($service_id_array);
        } 
        // SI LA SOLICITUD TRAE FOTO LA GUARDAMOS EN EL PROYECTO
    	if($request->hasFile('picture')){
    		$bedroom->uploadPicture($request->file('picture'),'picture','jpg','0','0','0');
    	}
        //CREAMOS EL MENSAJE
    	\Toastr::success(trans('strings.created', ['variable' => $bedroom->name]));
        // RETORNAMOS A LA VISTA
    	return redirect('admin/bedrooms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Bedroom $bedroom)
    {
        // RETORNAMOS A LA VISTA EL MODELO
    	return $bedroom;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bedroom $bedroom)
    {

        // TOMAMOS LOS PISOS DE LA BASE DE DATOS PAR MOSTRAR EN UN SELECT
    	$floors = Floor::get()->pluck('id', 'number');
        // TOMAMOS LOS SERVICIOS DE LA BASE DE DATOS PAR MOSTRAR EN UN SELECT
    	$bedroom_service = Bedroom::find($bedroom->id)->services()->pluck('service_id')->toArray();
        $services = Service::all()->pluck('name', 'id');
        // RETORNAMOS A LA VISTA EL MODELO
    	return view('app.admin.bedrooms.preform', compact('bedroom','floors','services','bedroom_service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bedroom $bedroom)
    {
        // VALIDACION DE LA SOLICITUD DESDE FORMULARIO
    	$request->validate([
    		'floor_id' => 'required',
            'number_people' => 'required',
    		'name' => 'required',
    		'description' => 'required',
    		'price' => 'required',
    	]);
        // ELIMINAMOS LOS SERVICIOS DE LA TABLA MUCHOS A MUCHOS
    	if($bedroom->services->count()) {
            $bedroom->services()->detach($bedroom->services()->pluck('service_id')->toArray());
        }
        // SI LA SOLICITUD TRAE SERVICIOS LOS GUARDAMOS EN LA TABLA MUCHOS A MUCHOS
    	$service_id_array = $request->input('service_id');
        if($service_id_array!=null){
            $bedroom->services()->attach($service_id_array);
        } 
        //ACTUALIZAMOS LA SOLICITUD EN LA BASE DE DATOS
        $bedroom->update($request->all());
        // GUARDAMOS LA FOTO SUBIDA EN EL PROYECTO
    	if($request->hasFile('picture')){
    		$bedroom->uploadPicture($request->file('picture'),'picture','jpg','0','0','0');
    	}
        //CREAMOS EL MENSAJE
    	\Toastr::success(trans('strings.updated', ['variable' => $bedroom->name]));
        // RETORNAMOS A LA VISTA
    	return redirect('admin/bedrooms/');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bedroom $bedroom)
    {
        //ELIMINAMOS LA SOLICITUD EN LA BASE DE DATOS
    	$bedroom->delete();
        //CREAMOS EL MENSAJE
    	\Toastr::success(trans('strings.deleted', ['variable' => $bedroom->name]));
        // RETORNAMOS A LA VISTA
    	return redirect()->to('/admin/bedrooms');
    }

    public function deleteMultiple(Request $request)
    {   
        // FUNCION PARA ELIMINAR MULTIPLE SOLICITUD DE CHECKBOX
        // BUSCAMOS EL REGISTRO A ELIMINAR
    	$bedrooms = Bedroom::find($request['id']);
        // CONTAMOS CUANTOS REGISTROS HAY POR ELIMINAR
    	$count = Bedroom::find($request['id'])->count();
        // ELIMINAMOS LOS REGISTROS
    	Bedroom::destroy($request['id']);
        // VARIABLE DE MENSAJE
    	$names = "";
        // RECORREMOS LOS REGISTROS PARA GUARDAR EL NUMERO O NOMBRE A MOSTRAR EN MENSAJE
    	foreach ($bedrooms as $bedroom) {
    		if($names==""){
    			$names = $bedroom->name;
    		}else{
    			$names = $names . ", " . $bedroom->name;
    		}
    	}
        // MOSTRAMOS EL MENSAJE SEGUN LA CANTIDAD DE REGISTROS A ELIMINAR
    	if($count == 1){
    		\Toastr::success(trans('strings.deleted', ['variable' => $names]));
    	}else{
    		\Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
    	}
    }
}
