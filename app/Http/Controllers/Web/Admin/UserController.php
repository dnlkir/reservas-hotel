<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\DataTables\Admin\UsersDataTable as DataTable;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        $title = trans('strings.users');
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $roles = Role::all()->pluck('display_name', 'id');
        return view('app.admin.users.preform', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $request['password'] = \Hash::make($request['password']);
        $user = User::create($request->all());
        $role_id_array = $request->input('role_id');
        if($role_id_array!=null){
            $user->attachRoles($role_id_array);
        }   
        \Toastr::success(trans('strings.created', ['variable' => $user->username]));
        if($request->hasFile('file')){
            $user->uploadPicture($request->file('file'));
        } 
        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles_user = User::find($user->id)->roles()->pluck('role_id')->toArray();
        $roles = Role::all()->pluck('display_name', 'id');
        return view('app.admin.users.preform', compact('user', 'roles', 'roles_user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request['password']=='' ? $request->except('password') : array(
            'name'          => $request['name'],
            'username'      => $request['username'],
            'email'         => $request['email'],
            'password'      => bcrypt($request['password']),
            'language'      => 'es',
            );

        if($user->roles->count()) {
            $user->roles()->detach($user->roles()->pluck('role_id')->toArray());
        }
        $role_id_array = $request->input('role_id');
        if($role_id_array!=null){
            $user->attachRoles($role_id_array);
        }

        $user->update($data);

        if($request->hasFile('file')){
            $user->uploadPicture($request->file('file'));
        }      

        \Toastr::success(trans('strings.updated', ['variable' => $user->username]));
        return redirect('admin/users');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        \Toastr::success(trans('strings.deleted', ['variable' => $user->username]));
        return redirect()->to('/admin/users');
    }

    public function deleteMultiple(Request $request)
    {   
        $users = User::find($request['id']);
        $count = User::find($request['id'])->count();
        User::destroy($request['id']);
        $names = "";
        foreach ($users as $user) {
            if($names==""){
                $names = $user->username;
            }else{
                $names = $names . ", " . $user->username;
            }
        }
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
