<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\DataTables\Admin\ServicesDataTable as DataTable;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        $title = 'Servicios';
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = new Service();
        return view('app.admin.services.preform', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$request->validate([
    		'name' => 'required'
    	]);
        $service = Service::create($request->all());
        \Toastr::success(trans('strings.created', ['variable' => $service->name]));
        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return $service;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('app.admin.services.preform', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
    	$request->validate([
    		'name' => 'required'
    	]);

        $service->update($request->all());
        \Toastr::success(trans('strings.updated', ['variable' => $service->name]));
        return redirect('admin/services/');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        \Toastr::success(trans('strings.deleted', ['variable' => $service->name]));
        return redirect()->to('/admin/services');
    }

    public function deleteMultiple(Request $request)
    {   
        $services = Service::find($request['id']);
        $count = Service::find($request['id'])->count();
        Service::destroy($request['id']);
        $names = "";
        foreach ($services as $service) {
            if($names==""){
                $names = $service->name;
            }else{
                $names = $names . ", " . $service->name;
            }
        }
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
