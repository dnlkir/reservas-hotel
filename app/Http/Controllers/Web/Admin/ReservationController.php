<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\Bedroom;
use App\DataTables\Admin\ReservationsDataTable as DataTable;
use DateTime;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        // TITULO A MOSTRAR EN VISTA
        $title = 'Reservas';
        // OCULTAMOS DE LA VISTA EL BOTON CREAR
        $HideCreate = false;
        // HABILITAR EN LA VISTA EL BOTON ELIMINAR MULTIPLE CON PADDING
        $HideMultipleDeletePb = true;
        // OCULTAMOS DE LA VISTA EL BOTON ELIMINAR MULTIPLE
        $HideMultipleDelete = false;
        // RETORNAMOS A LA VISTA
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title','HideCreate','HideMultipleDelete','HideMultipleDeletePb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //INSTANCIAMOS EL MODELO PARA DEVOLVERLO A LA VISTA
        $reservation = new Reservation();
        // RETORNAMOS A LA VISTA
        return view('app.admin.reservations.preform', compact('reservation'));
    }

    public function create_reservation($id)
    {
        //INSTANCIAMOS EL MODELO PARA DEVOLVERLO A LA VISTA
        $reservation = new Reservation();
        $bedroom = Bedroom::find($id);
        // RETORNAMOS A LA VISTA
        return view('app.admin.reservations.preform', compact('reservation','bedroom'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'bedroom_id' => 'required',
            'name' => 'required',
            'identification_document' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'entry_date' => 'required',
            'departure_date' => 'required',
        ]);

        $request['status'] = 'ocupada';

        //VERIFICAR SI LA HABITACIPON TIENE UNA RESERVA ACTIVA EN LAS FECHAS
        $from = date($request['entry_date']);
        $to = date($request['departure_date']);

        $bedrooms = Bedroom::
        where('id', $request['bedroom_id'])
        ->with(['reservations' => function ($query) use ($request, $from, $to) {
            $query->where(function ($query) {
                $query->where('status', '=', 'reserva')
                ->orWhere('status', '=', 'ocupada');
            });
            $query->where(function ($query) use ($request, $from, $to){
                $query->whereBetween('entry_date', [$from, $to]);
                $query->orWhereBetween('departure_date', [$from, $to]);
            });
        }])
        ->first();

        if(count($bedrooms->reservations)){
            \Toastr::warning('la habitación ya cuenta con una reserva en el rango de fechas');
            return redirect('admin/reservations/'.$request['bedroom_id'].'/create_reservation')->with(compact($request));     
        }else{
            $reservation = Reservation::create($request->all());
            \Toastr::success('la reserva para '.$reservation->name. ' fue guardada exitosamente');
            return redirect('admin/reservations/');  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return $reservation;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        return view('app.admin.reservations.preform', compact('reservation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
    	$request->validate([
    		'status' => 'required'
    	]);

        $reservation->update($request->all());
        \Toastr::success(trans('strings.updated', ['variable' => $reservation->name]));
        return redirect('admin/reservations/');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->delete();
        \Toastr::success(trans('strings.deleted', ['variable' => $reservation->name]));
        return redirect()->to('/admin/reservations');
    }

    public function deleteMultiple(Request $request)
    {   
        $reservations = Reservation::find($request['id']);
        $count = Reservation::find($request['id'])->count();
        Reservation::destroy($request['id']);
        $names = "";
        foreach ($reservations as $reservation) {
            if($names==""){
                $names = $reservation->name;
            }else{
                $names = $names . ", " . $reservation->name;
            }
        }
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
