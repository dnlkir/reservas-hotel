<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bedroom;
use App\Models\Reservation;
use DateTime;

class AvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //CONSULTO LAS HABITACIONES CON LAS RESERVAS DEL DIA DE HOY
        $bedrooms = Bedroom::with(['reservations_availability' => function ($query) {
            //FECHA DEL SISTEMA, HOY
            $date = new DateTime('now');
            $date = $date->format('Y-m-d');
            //CONSULTA CON LAS FECHAS DEL SISTEMA
            $query->where('entry_date', '<=', $date);
            $query->where('departure_date', '>=', $date);

        }])->get();

        // RETORNAMOS A LA VISTA
        return view('app.admin.availabilitys.preform', compact('bedrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //INSTANCIAMOS EL MODELO PARA DEVOLVERLO A LA VISTA
        $availability = new Availability();
        // RETORNAMOS A LA VISTA
        return view('app.admin.availabilitys.preform', compact('availability'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // VALIDACION DE LA SOLICITUD DESDE FORMULARIO
    	$request->validate([
    		'number' => 'required|numeric'
    	]);
        //CREAMOS LA SOLICITUD EN LA BASE DE DATOS
        $availability = Availability::create($request->all());
        //CREAMOS EL MENSAJE
        \Toastr::success(trans('strings.created', ['variable' => $availability->number]));
        // RETORNAMOS A LA VISTA
        return redirect('admin/availabilitys');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Availability $availability)
    {
        // RETORNAMOS A LA VISTA EL MODELO
        return $availability;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Availability $availability)
    {
        // RETORNAMOS A LA VISTA EL MODELO
        return view('app.admin.availabilitys.preform', compact('availability'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Availability $availability)
    {
        // VALIDACION DE LA SOLICITUD DESDE FORMULARIO
    	$request->validate([
    		'number' => 'required|numeric'
    	]);
        //ACTUALIZAMOS LA SOLICITUD EN LA BASE DE DATOS
        $availability->update($request->all());
        //CREAMOS EL MENSAJE
        \Toastr::success(trans('strings.updated', ['variable' => $availability->number]));
        // RETORNAMOS A LA VISTA
        return redirect('admin/availabilitys/');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Availability $availability)
    {
        //ELIMINAMOS LA SOLICITUD EN LA BASE DE DATOS
        $availability->delete();
        //CREAMOS EL MENSAJE
        \Toastr::success(trans('strings.deleted', ['variable' => $availability->number]));
        // RETORNAMOS A LA VISTA
        return redirect()->to('/admin/availabilitys');
    }

    public function deleteMultiple(Request $request)
    {   
        // FUNCION PARA ELIMINAR MULTIPLE SOLICITUD DE CHECKBOX
        // BUSCAMOS EL REGISTRO A ELIMINAR
        $availabilitys = Availability::find($request['id']);
        // CONTAMOS CUANTOS REGISTROS HAY POR ELIMINAR
        $count = Availability::find($request['id'])->count();
        // ELIMINAMOS LOS REGISTROS
        Availability::destroy($request['id']);
        // VARIABLE DE MENSAJE
        $names = "";
        // RECORREMOS LOS REGISTROS PARA GUARDAR EL NUMERO O NOMBRE A MOSTRAR EN MENSAJE
        foreach ($availabilitys as $availability) {
            if($names==""){
                $names = $availability->number;
            }else{
                $names = $names . ", " . $availability->number;
            }
        }
        // MOSTRAMOS EL MENSAJE SEGUN LA CANTIDAD DE REGISTROS A ELIMINAR
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
