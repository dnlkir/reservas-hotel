<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\DataTables\Admin\HistoricsDataTable as DataTable;

class HistoricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        // TITULO A MOSTRAR EN VISTA
        $title = 'Historial';
        // OCULTAMOS DE LA VISTA EL BOTON CREAR
        $HideCreate = false;
        // OCULTAMOS DE LA VISTA EL BOTON ELIMINAR MULTIPLE
        $HideMultipleDelete = false;
        // RETORNAMOS A LA VISTA
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title','HideCreate','HideMultipleDelete'));
    }
}
