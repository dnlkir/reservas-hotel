<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Admin\RequestLogsDataTable as DataTable;

class RequestLogController extends Controller
{
    public function index(DataTable $dataTable){
    	$title = trans('strings.request_logs');
    	$HideMultipleDelete = true;
    	$HideCreate = true;
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title', 'HideMultipleDelete', 'HideCreate'));
    }
}
