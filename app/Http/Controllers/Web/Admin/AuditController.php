<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Admin\AuditDataTable as DataTable;

class AuditController extends Controller
{
    public function index(DataTable $dataTable){
    	$title = trans('strings.audits');
    	$HideMultipleDelete = true;
        $HideCreate = true;
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title', 'HideMultipleDelete', 'HideCreate'));
    }

}
