<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\DataTables\Admin\RoleDataTable as DataTable;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        $title = trans('strings.roles');
        return $dataTable->render('metronic_layouts.partials.datatable', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = new Role();
        $roles = Role::all()->pluck('display_name', 'id');
        return view('app.admin.roles.preform', compact('role', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $role = Role::create($request->all());
        \Toastr::success(trans('strings.created', ['variable' => $role->name]));
        return redirect('admin/roles/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return $role;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('app.admin.roles.preform', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->all());
        \Toastr::success(trans('strings.updated', ['variable' => $role->name]));
        return redirect('admin/roles/');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        \Toastr::success(trans('strings.deleted', ['variable' => $role->name]));
        return redirect()->to('/admin/roles');
    }

    public function deleteMultiple(Request $request)
    {   
        $roles = Role::find($request['id']);
        $count = Role::find($request['id'])->count();
        Role::destroy($request['id']);
        $names = "";
        foreach ($roles as $role) {
            if($names==""){
                $names = $role->name;
            }else{
                $names = $names . ", " . $role->name;
            }
        }
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
