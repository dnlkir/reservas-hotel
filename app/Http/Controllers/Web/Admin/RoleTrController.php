<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\RoleTr;
use App\Http\Requests\RoleTrRequest;
use App\DataTables\Admin\RoleTrDataTable as DataTable;

class RoleTrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Role $role, DataTable $dataTable)
    {
        $title = sprintf("%s - %s", $role->name, trans('strings.translations'));
        return $dataTable
                ->forModel($role->id)
                ->render('metronic_layouts.partials.datatable', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Role $role)
    {
        $role_tr = new RoleTr;
        return view('app.admin.roles.role_trs.preform', compact('role_tr', 'role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleTrRequest $request, Role $role)
    {
        $role_tr = RoleTr::create($request->all());
        \Toastr::success(trans('strings.created', ['variable' => sprintf('%s - %s', $role_tr->role->name, $role_tr->display_name)]));
        return redirect(sprintf('/admin/roles/%s/role_trs', $role->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role, RoleTr $role_tr)
    {
        return $role_tr;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role, RoleTr $role_tr)
    {
        return view('app.admin.roles.role_trs.preform', compact('role_tr', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleTrRequest $request, Role $role, RoleTr $role_tr)
    {
        $role_tr->update($request->all());
        \Toastr::success(trans('strings.updated', ['variable' => sprintf('%s - %s', $role_tr->role->name, $role_tr->display_name)]));
        return redirect(sprintf('/admin/roles/%s/role_trs', $role->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role, RoleTr $role_tr)
    {
        $role_tr->delete();
        \Toastr::success(trans('strings.deleted', ['variable' => sprintf('%s - %s', $role_tr->role->name, $role_tr->display_name)]));
        return redirect(sprintf('/admin/roles/%s/role_trs', $role->id));
    }

    public function deleteMultiple(Request $request, Role $role)
    {   
        $role_trs = RoleTr::find($request['id']);
        $count = RoleTr::find($request['id'])->count();
        RoleTr::destroy($request['id']);
        $names = "";
        foreach ($role_trs as $role_tr) {
            if($names==""){
                $names = sprintf('%s - %s', $role_tr->role->name, $role_tr->display_name);
            }else{
                $names = $names . ", " . sprintf('%s - %s', $role_tr->role->name, $role_tr->display_name);
            }
        }
        if($count == 1){
            \Toastr::success(trans('strings.deleted', ['variable' => $names]));
        }else{
            \Toastr::success(trans('strings.deleted_plural', ['variable' => $names]));
        }
    }
}
