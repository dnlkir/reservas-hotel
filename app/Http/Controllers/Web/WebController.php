<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bedroom;
use App\Models\Reservation;
use DateTime;

class WebController extends Controller
{
	public function reservation(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'identification_document' => 'required',
			'phone' => 'required|numeric',
			'email' => 'required|email',
			'entry_date' => 'required|date',
			'departure_date' => 'required|date',
			'amount_people' => 'required|numeric',
			'information_accompanist' => 'required',
		]);

		$from = date($request['entry_date']);
		$to = date($request['departure_date']);

		$bedrooms = Bedroom::
		with('floor')
		->with('services')
		->with(['reservations' => function ($query) use ($request, $from, $to) {
			$query->whereBetween('entry_date', [$from, $to]);
			$query->orWhereBetween('departure_date', [$from, $to]);
		}])
		->where('number_people', $request['amount_people'])->get();

		//CALCULAR PRECIO HABITACION SEGUN DIAS
		$entry_date = new DateTime($request['entry_date']);
		$departure_date = new DateTime($request['departure_date']);

		$interval = $entry_date->diff($departure_date);
		$interval = $interval->format('%a') + 1;

		//RECORRO LAS HABITACIONES Y LES GUARDO EL PRECIO CALCULADO A CADA UNA
		foreach ($bedrooms as $key => $bedroom) {
			$bedroom['total_price'] = intval($bedroom['price']) * intval($interval);
		}

		return view('app.user.form', compact('bedrooms', 'request'));
	}

	public function save_reservation(Request $request)
	{
		$request->validate([
			'bedroom_id' => 'required',
			'name' => 'required',
			'identification_document' => 'required',
			'phone' => 'required|numeric',
			'email' => 'required|email',
			'entry_date' => 'required|date',
			'departure_date' => 'required|date',
			'amount_people' => 'required|numeric',
			'information_accompanist' => 'required',
		]);

		$bedroom = Bedroom::find($request['bedroom_id']);

		$entry_date = new DateTime($request['entry_date']);
		$departure_date = new DateTime($request['departure_date']);

		$interval = $entry_date->diff($departure_date);
		$interval = $interval->format('%a') + 1;


		$request['total_price'] = $bedroom->price * intval($interval);

		$request['status'] = 'reserva';
		$reservation = Reservation::create($request->all());
		\Toastr::success("La Habitacion para " .$reservation->name. " ha sido reservada");
		return redirect('/');
		
	}
}
