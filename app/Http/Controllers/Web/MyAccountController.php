<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\UserRequest;

class MyAccountController extends Controller
{
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $user = Auth::user();
        return view('app.my_account.index', compact('user'));
    }

    public function update(UserRequest $request){
        $user = Auth::user();

        if(!empty($request['password'])){
            $this->validate($request, [
                'password_current' => 'required',
            ]);

            if($request->has('password_current') && $request->has('password') && $request->has('password_confirmation')){
                if (!\Hash::check($request['password_current'], $user->password))
                {
                    \Toastr::error(trans('strings.password_current_error'));
                    return redirect('/my_account');
                }
            }
        }        
        
        if ($request->hasFile('file')){    
            $user->uploadPicture($request->file('file'));
        }        

        $data = $request['password']=='' ? $request->except('password') : array(
            'name'          => $request['name'],
            'username'      => $request['username'],
            'password'      => bcrypt($request['password']),
            );
        $user->update($data);

        \Toastr::success(trans('strings.account_updated'));
        return redirect('/my_account');
    }
}
