<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Auth;
use Session;
use Config;

class Language
{
    public function handle($request, Closure $next, $guard = null)
    {
    	if(Auth::guest()){
    		if (Session::has('appLocale') AND array_key_exists(Session::get('appLocale'), Config::get('languages'))) {
                App::setLocale(Session::get('appLocale'));
            }else{
                $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
                App::setLocale($locale);
                Session::put('appLocale', $locale);
            }
    	}else{
			if (Auth::guard($guard)->check()) {
				$locale = Auth::user()->language;
				App::setLocale($locale);
                Session::put('appLocale', $locale);
			}
    	}
    	
        return $next($request);
    }
}