<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleTr extends Model
{
	protected $primaryKey = 'id';

	protected $fillable = [
		'role_id',
		'display_name',
		'description',
		'locale'
	];

	/**
	* The attributes that should be hidden for arrays.
	*
	* @var array
	*/
	protected $hidden = [
		'created_at', 'updated_at', 'deleted_at'
	];

	public function role(){
		return $this->belongsTo(Role::class);
	}
}
