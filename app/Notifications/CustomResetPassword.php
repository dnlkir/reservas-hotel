<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomResetPassword extends Notification implements ShouldQueue
{
    use Queueable;
    
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(trans('mail.hello'))
            ->salutation(sprintf("%s, %s", trans('mail.regards'), config('app.name')))
            ->subject(trans('mail.reset_password_subject'))
            ->line(trans('mail.reset_password_line_1'))
            ->action(trans('mail.reset_password_action'), url('password/reset', $this->token))
            ->line(trans('mail.reset_password_line_2'));
    }
}
