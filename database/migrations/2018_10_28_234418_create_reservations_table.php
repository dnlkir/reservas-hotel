<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bedroom_id');
            $table->string('name');
            $table->string('identification_document')->nullable();
            $table->string('amount_people');
            $table->string('phone');
            $table->string('email');
            $table->string('status');
            $table->date('entry_date');
            $table->date('departure_date');
            $table->double('total_price');
            $table->text('information_accompanist');
            $table->timestamps();

            $table->foreign('bedroom_id')->references('id')->on('bedrooms')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
