<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bedrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number_people');
            $table->unsignedInteger('floor_id');
            $table->string('name');
            $table->string('description');
            $table->integer('price');
            $table->string('picture');
            $table->timestamps();

            $table->foreign('floor_id')->references('id')->on('floors')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bedrooms');
        // Schema::table('bedrooms', function (Blueprint $table) {
        //     $table->dropForeign(['floor_id']);
        //     $table->dropColumn(['country_id','stateprovince_id','city_id']);
        // });
    }
}
