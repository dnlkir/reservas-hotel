<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedroomServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bedroom_service', function (Blueprint $table) {
            $table->unsignedInteger('bedroom_id');
            $table->unsignedInteger('service_id');

            $table->foreign('service_id')->references('id')->on('services')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('bedroom_id')->references('id')->on('bedrooms')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['bedroom_id', 'service_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bedroom_service');
    }
}
