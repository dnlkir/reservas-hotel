<?php

use Illuminate\Database\Seeder;
use App\RoleTr;

class RoleTrsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

		RoleTr::create([
			'role_id'		=> 1,
			'display_name'	=> 'Administrator',
			'description'	=> 'Description for Admin',
			'locale'		=> 'en',
		]);

		RoleTr::create([
			'role_id'		=> 1,
			'display_name'	=> 'Administrador',
			'description'	=> 'Descripción de Admin',
			'locale'		=> 'es',
		]);
	}
}
