<?php

use Illuminate\Database\Seeder;
use App\Models\Reservation;

class ReservationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reservation::create([
        // 	'bedroom_id' => 2,
        // 	'name' => 'cliente uno',
        // 	'amount_people' => '2',
        // 	'phone' => '3140000000',
        // 	'email' => 'clienteuno@hotmail.com',
        // 	'status' => 'reserva',
        // 	'entry_date' => '2018-11-9',
        //     'departure_date' => '2018-11-9',
        // 	'total_price' => '50000'
        // ]);

        // Reservation::create([
        //     'bedroom_id' => 6,
        //     'name' => 'cliente dos',
        //     'amount_people' => '2',
        //     'phone' => '3140000000',
        //     'email' => 'clientedos@hotmail.com',
        //     'status' => 'ocupada',
        //     'entry_date' => '2018-11-9',
        //     'departure_date' => '2018-11-9',
        //     'total_price' => '50000'
        // ]);

        // Reservation::create([
        //     'bedroom_id' => 10,
        //     'name' => 'cliente tres',
        //     'amount_people' => '2',
        //     'phone' => '3140000000',
        //     'email' => 'clientetres@hotmail.com',
        //     'status' => 'entregada',
        //     'entry_date' => '2018-11-5',
        //     'departure_date' => '2018-11-5',
        //     'total_price' => '50000'
        // ]);
    }
}
