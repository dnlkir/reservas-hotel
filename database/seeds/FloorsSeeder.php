<?php

use Illuminate\Database\Seeder;
use App\Models\Floor;

class FloorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Floor::create([
        	'number' => '1'
        ]);

        Floor::create([
        	'number' => '2'
        ]);

        Floor::create([
        	'number' => '3'
        ]);

        Floor::create([
        	'number' => '4'
        ]);

    }
}
