<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::create([
    		'id'		=> 1,
    		'name'		=> 'José Rodolfo Ruiz aranda',
    		'username'	=> 'joserodolfo',
    		'email'		=> 'joseruiz160434@gmail.com',
    		'picture'	=> '/img/profile-placeholder.jpg',
    		'password'	=> \Hash::make('123456'),
    		'language'	=> 'es'
    		]);

        $user->roles()->attach([1]);
    }
}
