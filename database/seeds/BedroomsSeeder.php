<?php

use Illuminate\Database\Seeder;
use App\Models\Bedroom;
class BedroomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bedroom = Bedroom::create([
            'floor_id' => 1,
            'number_people' => 1,
            'name' => '101',
            'description' => 'Habitación amoblada con cama doble y escritorio',
            'price' => '25000',
            'picture' => '/storage/pictures/1.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
        	'floor_id' => 1,
            'number_people' => 2,
            'name' => '102',
            'description' => 'Habitacíón 102',
            'price' => '25000',
            'picture' => '/storage/pictures/2.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
        	'floor_id' => 1,
            'number_people' => 3,
            'name' => '103',
            'description' => 'Habitacíón 103',
            'price' => '25000',
            'picture' => '/storage/pictures/3.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
        	'floor_id' => 1,
            'number_people' => 4,
            'name' => '104',
            'description' => 'Habitacíón 104',
            'price' => '30000',
            'picture' => '/storage/pictures/4.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);




        $bedroom = Bedroom::create([
            'floor_id' => 2,
            'number_people' => 1,
            'name' => '201',
            'description' => 'Habitacíón 201',
            'price' => '30000',
            'picture' => '/storage/pictures/1.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 2,
            'number_people' => 2,
            'name' => '202',
            'description' => 'Habitacíón 202',
            'price' => '30000',
            'picture' => '/storage/pictures/2.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 2,
            'number_people' => 3,
            'name' => '203',
            'description' => 'Habitacíón 203',
            'price' => '35000',
            'picture' => '/storage/pictures/3.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 2,
            'number_people' => 4,
            'name' => '204',
            'description' => 'Habitacíón 204',
            'price' => '35000',
            'picture' => '/storage/pictures/4.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);





        $bedroom = Bedroom::create([
            'floor_id' => 3,
            'number_people' => 1,
            'name' => '301',
            'description' => 'Habitacíón 301',
            'price' => '40000',
            'picture' => '/storage/pictures/1.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 3,
            'number_people' => 2,
            'name' => '302',
            'description' => 'Habitacíón 302',
            'price' => '40000',
            'picture' => '/storage/pictures/2.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 3,
            'number_people' => 3,
            'name' => '303',
            'description' => 'Habitacíón 303',
            'price' => '40000',
            'picture' => '/storage/pictures/3.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 3,
            'number_people' => 4,
            'name' => '304',
            'description' => 'Habitacíón 304',
            'price' => '40000',
            'picture' => '/storage/pictures/4.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);





        $bedroom = Bedroom::create([
            'floor_id' => 4,
            'number_people' => 1,
            'name' => '401',
            'description' => 'Habitacíón 401',
            'price' => '45000',
            'picture' => '/storage/pictures/1.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 4,
            'number_people' => 2,
            'name' => '402',
            'description' => 'Habitacíón 402',
            'price' => '45000',
            'picture' => '/storage/pictures/2.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 4,
            'number_people' => 3,
            'name' => '403',
            'description' => 'Habitacíón 403',
            'price' => '45000',
            'picture' => '/storage/pictures/3.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

        $bedroom = Bedroom::create([
            'floor_id' => 4,
            'number_people' => 4,
            'name' => '404',
            'description' => 'Habitacíón 404',
            'price' => '45000',
            'picture' => '/storage/pictures/4.jpg',
        ]);

        $bedroom->services()->attach([1,2,3,4]);

    }
}
