<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(RoleTrsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(FloorsSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(BedroomsSeeder::class);
        $this->call(ReservationsSeeder::class);
    }
}
