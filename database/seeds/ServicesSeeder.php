<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
        	'name' => 'Agua caliente',
        ]);

        Service::create([
        	'name' => 'Cama doble',
        ]);

        Service::create([
        	'name' => 'Balcon',
        ]);

        Service::create([
        	'name' => 'TV',
        ]);

        Service::create([
        	'name' => 'Internet',
        ]);

         Service::create([
            'name' => 'Servicio de desayuno',
        ]);

          Service::create([
            'name' => 'Aire acondicionado',
        ]);

           Service::create([
            'name' => 'Bañera',
        ]);

             Service::create([
            'name' => 'Escritorio',
        ]);

           Service::create([
            'name' => 'Caja de seguridad',
        ]);       
    }
}
